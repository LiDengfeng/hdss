# -*- coding: utf-8 -*-
import os, sys
try:
    this_path = os.path.dirname(os.path.abspath(__file__))
except:
    this_path = os.path.dirname(os.path.abspath("__file__"))
src_path = os.path.join(this_path, "src")
if src_path not in sys.path: sys.path.append(src_path)

from main import main

if __name__ == '__main__':
    main(sys.argv)