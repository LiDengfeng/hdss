# -*- coding: utf-8 -*-
"""
网易云音乐
"""

import base64
import json
import os
import re
import music
from libs import request as myrequest

from Crypto.Cipher import AES
from binascii import hexlify

baseUrl = 'https://music.163.com'


class Encrypyed():
    def __init__(self):
        # 加密的固有参数
        self.pub_key = "010001"
        self.modulus = "00e0b509f6259df8642dbc35662901477df22677ec152b5ff6" \
                       "8ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee34" \
                       "1f56135fccf695280104e0312ecbda92557c93870114af6c9d05c" \
                       "4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e820" \
                       "47b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7"
        self.nonce = "0CoJUm6Qyw8W8jud"

    # 随机产生16位参数
    def a(self, size):
        return hexlify(os.urandom(size))[:16].decode('utf-8')

    # 加密
    def b(self, text, key):
        iv = b'0102030405060708'
        pad = 16 - len(text) % 16
        text = text + pad * chr(pad)
        encryptor = AES.new(str.encode(key), AES.MODE_CBC, iv)
        result = encryptor.encrypt(str.encode(text))
        result_str = base64.b64encode(result).decode('utf-8')
        return result_str

    # 产生第二个参数
    def c(self, text, pubKey, modulus):
        text = text[::-1]
        rs = pow(int(hexlify(text.encode('utf-8')), 16), int(pubKey, 16), int(modulus, 16))
        return format(rs, 'x').zfill(256)

    # 赋值加密
    def d(self, text):
        text = json.dumps(text)
        i = self.a(16)
        encText = self.b(text, self.nonce)
        encText = self.b(encText, i)
        encSecKey = self.c(i, self.pub_key, self.modulus)
        data = {'params': encText, 'encSecKey': encSecKey}
        return data


def get_page_num():
    """
    获取页数
    :return:
    """
    startUrl = 'https://music.163.com/discover/playlist/?order=new'
    html = myrequest.get_html(startUrl)
    pageNum = re.findall(r'<span class="zdot".*?class="zpgi">(.*?)</a>', html, re.S)[0]
    return int(pageNum)


def get_music_playlist(page):
    """
    获取音乐列表
    :param page: 页数
    :return:
    """
    startUrl = 'https://music.163.com/discover/playlist/?order=new&cat=%E5%85%A8%E9%83%A8&limit=35&offset=' + str(page)
    html = myrequest.get_html(startUrl)
    pattern = re.compile(
        '<li>.*?img class="j-flag" src="(.*?)">.*?<p.*?class="dec">.*?<.*?title="(.*?)".*?href="(.*?)".*?>.*?span class="s-fc4".*?title="(.*?)".*?href="(.*?)".*?</li>',
        re.S)
    with open("toplist.html", "w") as f:
        f.write(html)
    result = re.findall(pattern, html)
    info = []
    # 对第一页的歌单获取想要的信息
    for i in result:
        data = {}
        data['cover'] = i[0]
        data['title'] = i[1]
        url = baseUrl + i[2]
        data['url'] = url
        data['author'] = i[3]
        data['authorUrl'] = baseUrl + i[4]
        pattern = re.compile(r'.*?id=(\w+)', re.S)
        m = pattern.match(i[2])
        data['id'] = m.group(1)
        info.append(data)
    return info


def get_music_playlist_list(playlist_id):
    """
    获取列表中的歌曲列表
    :param playlist_id:
    :return:
    """
    startUrl = 'https://music.163.com/playlist?id=' + str(playlist_id)
    html = myrequest.get_page(startUrl, frame='contentFrame', waitid='song-list-pre-cache')
    pattern = re.compile(
        r'<table class="m-table ">.*?<tbody>(.*?)</tbody>.*?</table>',
        re.S)
    result = re.findall(pattern, html)
    content = result[0]
    pattern = re.compile(
        r'<tr id="(.*?)".*?<span class="txt">.*?<a href="(.*?)".*?<b title="(.*?)">.*?<span class="u-dur ">(.*?)</span>.*?<div class="text" title="(.*?)">.*?<div class="text"><a href="(.*?)" title="(.*?)">.*?</tr>',
        re.S)
    result = re.findall(pattern, content)
    info = []
    # 对第一页的歌单获取想要的信息
    for i in result:
        d = music.Music()
        d.source = '163'
        d.song_name = i[2]
        d.artist_name = i[4]
        d.album_name = i[6]
        pattern = re.compile(r'.*?id=(\w+)', re.S)
        m = pattern.match(i[2])
        d.song_key = '163*$$*' + str(m.group(1))
        d.id = str(m.group(1))
        info.append(d)
    return info


def get_music_toplist_list():
    """
    获取榜单前100
    :return:
    """
    startUrl = 'https://music.163.com/discover/toplist'
    html = myrequest.get_page(startUrl, frame='contentFrame', waitid='song-list-pre-cache')
    pattern = re.compile(
        r'<table class="m-table m-table-rank">.*?<tbody>(.*?)</tbody>.*?</table>',
        re.S)
    result = re.findall(pattern, html)
    content = result[0]
    pattern = re.compile(
        r'<tr id="(.*?)".*?<span class="txt">.*?<a href="(.*?)".*?<.*? title="(.*?)">.*?<span class="u-dur ">(.*?)</span>.*?<div class="text" title="(.*?)">.*?<a class="" href="(.*?)".*?</tr>',
        re.S)
    result = re.findall(pattern, content)
    info = []
    for i in result:
        d = music.Music()
        d.source = '163'
        d.song_name = i[2]
        d.artist_name = i[4]
        pattern = re.compile(r'.*?id=(\w+)', re.S)
        m = pattern.match(i[1])
        d.song_key = '163*$$*' + str(m.group(1))
        d.id = str(m.group(1))
        info.append(d)

    return info


def search_music(key, page=0, limit=1):
    """
    搜索音乐
    :return:
    """
    headers = {
        'origin': 'https://music.163.com',
        'referer': 'https://music.163.com/search/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
    }
    # 构造请求字典
    query = {
        "s": key,
        "offset": page,
        "limit": limit,
        "type": "1",
    }
    url = 'http://music.163.com/weapi/cloudsearch/get/web?csrf_token='
    # 解密
    do = Encrypyed()
    # 请求参数
    data = do.d(query)
    html = myrequest.post(url, data=data, headers=headers)
    total_num = 0
    song_list = []
    if html and html.status_code == 200:
        jd = html.json()
        if jd and 'code' in jd and jd['code'] == 200:
            if 'result' in jd and 'songs' in jd['result']:
                total_num = jd['result']['songCount']
                for x in jd['result']['songs']:
                    d = music.Music()
                    d.source = '163'
                    d.song_name = x['name']
                    d.artist_name = x['ar'][0]['name']
                    d.album_name = x['al']['name']
                    d.song_key = '163*$$*' + str(x['id'])
                    d.album_key = '163*$$*' + str(x['al']['id'])
                    d.artist_key = '163*$$*' + str(x['ar'][0]['id'])
                    d.id = str(x['id'])
                    song_list.append(d)
    return (total_num, song_list)


def get_music_resource(song_key):
    """
    搜索真实url地址
    :param song_key:
    :return:
    """
    if "*$$*" in song_key:
        songid = song_key.split("*$$*")[1]
    else:
        songid = song_key

    try:
        headers = {
            'origin': 'https://music.163.com',
            'referer': 'https://music.163.com/',
            'user - agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
        }
        query = {
            'csrf_token': "",
            'encodeType': "aac",
            'ids': "[" + str(songid) + "]",
            'level': "standard"
        }
        url = 'https://music.163.com/weapi/song/enhance/player/url/v1?csrf_token='
        # 解密
        do = Encrypyed()
        # 请求参数
        data = do.d(query)
        html = myrequest.post(url, data=data, headers=headers)
        jd = html.json()
        if jd and 'code' in jd and jd['code'] == 200 and 'data' in jd:
            return jd['data'][0]['url'], jd['data'][0]['type']
        else:
            songurl = 'http://music.163.com/song/media/outer/url?id=%s.mp3' % (str(songid))
            return songurl, 'mp3'
    except:
        songurl = 'http://music.163.com/song/media/outer/url?id=%s.mp3' % (str(songid))
        return songurl, 'mp3'


def test1():
    d = get_music_playlist(0)
    result = {}
    x = d[0]
    result[x['id']] = get_music_playlist_list(x['id'])
    return result


def test2():
    '''
        根据chrome浏览器2017年发布的新特性,
        需要unix版本的chrome版本高于57,
        windows版本的chrome版本高于58,
        才能使用无界面运行.
    '''
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC

    import time

    chrome_opt = Options()  # 创建参数设置对象.
    chrome_opt.add_argument('--headless')  # 无界面化.
    chrome_opt.add_argument('--disable-gpu')  # 配合上面的无界面化.
    # chrome_opt.add_argument('--window-size=1366,768')  # 设置窗口大小, 窗口大小会有影响.
    # chrome_opt.add_argument('–-disable-infobars') #禁用浏览器正在被自动化程序控制的提示

    # 创建Chrome对象并传入设置信息.
    driver = webdriver.Chrome(chrome_options=chrome_opt)
    # 操作这个对象.
    driver.get('https://music.163.com/playlist?id=2960078996')  # get方式访问百度.
    driver.switch_to.frame('contentFrame')
    try:
        # 创建wait对象.
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'song-list-pre-cache')))
        html = driver.page_source
        pattern = re.compile(
            r'<table class="m-table ">.*?<tbody>(.*?)</tbody>.*?</table>',
            re.S)
        result = re.findall(pattern, html)
        content = result[0]
        pattern = re.compile(
            r'<tr id="(.*?)".*?<span class="txt">.*?<a href="(.*?)".*?<b title="(.*?)">.*?<span class="u-dur ">(.*?)</span>.*?<div class="text" title="(.*?)">.*?<div class="text"><a href="(.*?)" title="(.*?)">.*?</tr>',
            re.S)
        result = re.findall(pattern, content)
    finally:
        driver.quit()

def mytest():
    #get_music_toplist_list()
    #test1()
    (count, songs) = search_music('遇到', limit=10)
    for x in songs:
        print (x)
        print(get_music_resource(x.song_key))

if __name__ == '__main__':
    mytest()
