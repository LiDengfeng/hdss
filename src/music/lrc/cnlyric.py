#coding=utf-8
import os
import sys
sys.path.append('..')
import re
from libs.convert import encoding
from libs.utils import *

pattern = re.compile(r'<div class="box">(.*?)</div>', re.DOTALL)
item_pattern = re.compile(r'<a.*?>(.*?)</a>.*?<a.*?>(.*?)</a>.*<a href="(.*?).lrc"', re.DOTALL)

def format(s):
	return s.replace('<strong style="color:red">','').replace('</strong>','')

def parse(data):
	#print data
	items = pattern.findall(data)
	result = []
	if items:
		for item in items:
			match = item_pattern.findall(item)
			for info in match:
				songinfo = {
				'song_name': encoding.to_utf8(format(info[0])),
				'artist_name': encoding.to_utf8(format(info[1])),
				'album_name': '',
				'lrc': 'http://www.cnlyric.com/' + info[2] + '.lrc',
				}
				result.append(songinfo)
		pass
	return result



def search(songname, artist):
	if artist:
		theurl = 'http://www.cnlyric.com/search.php?k=%s+%s&t=s' %(encoding.to_gb2312(songname), encoding.to_gb2312(artist))
	else:
		theurl = 'http://www.cnlyric.com/search.php?k=%s&t=s' % encoding.to_gb2312(songname)

	data = getUrlData(theurl)
	#print unicode(data,'iso-8859-2', ignore=True)
	if data:
		return parse(data)
	return []

if __name__ == '__main__':
	result =  search('爱情', '王')
	print '共找到:', len(result)
	print '-------------------------------'
	i = 1
	for s in result:
		print '第', i, '首:'
		print '歌名：', encoding.to_utf8(s['song_name'])
		print '艺人：', encoding.to_utf8(s['artist_name'])
		print '专辑：', encoding.to_utf8(s['album_name'])
		print 'LRC: ', encoding.to_utf8(s['lrc'])
		print
		i += 1
		pass
	pass






