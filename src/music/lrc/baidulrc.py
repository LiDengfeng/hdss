# -*- coding: utf-8 -*-
import os
import sys

sys.path.append('..')
import re
import json
from libs.convert import encoding
from libs.utils import *


def getdownurl(songId):
    song_id = int(songId)
    return 'http://music.qq.com/miniportal/static/lyric/%i/%i.xml' % (song_id % 100, song_id)


def parse(data):
    data = data[15:-2]

    outdata = re.sub(r"(,?)(\w+?)\s*?:", r"\1'\2':", data)
    outdata = outdata.replace("'", "\"")
    result = []
    try:
        jsonobj = json.loads(outdata)
        songlist = jsonobj['songlist']

        for s in songlist:
            songId = int(s['song_id'])
            songinfo = {
                'song_name': encoding.to_utf8(s['song_name']),
                'artist_name': encoding.to_utf8(s['singer_name']),
                'album_name': encoding.to_utf8(s['album_name']),
                'lrc': getdownurl(songId)
            }
            result.append(songinfo)
            pass
    except Exception, e:
        print e
    return result


def search(songname, artist):
    song_name = encoding.to_gb2312(songname)
    if artist:
        artist_name = encoding.to_gb2312(artist)
        url = 'http://shopcgi.qqmusic.qq.com/fcgi-bin/shopsearch.fcg?out=json&value=%s=qry_song&page_no=1&page_record_num=20&uin=0&artist=%s' % (
        song_name, artist_name)
    else:
        url = 'http://shopcgi.qqmusic.qq.com/fcgi-bin/shopsearch.fcg?out=json&value=%s=qry_song&page_no=1&page_record_num=20&uin=0' % (
            song_name)
    print repr(url)
    data = getUrlData(url)
    if data:
        return parse(data)
    pass


def main():
    result = search('我喜欢你', '王羚柔') or []
    print '共找到:', len(result)
    print '-------------------------------'
    i = 1
    for s in result:
        print '第', i, '首:'
        print '歌名：', encoding.to_utf8(s['song_name'])
        print '艺人：', encoding.to_utf8(s['artist_name'])
        print '专辑：', encoding.to_utf8(s['album_name'])
        print 'LRC: ', encoding.to_utf8(s['lrc'])
        print
        i += 1
        pass
    pass


if __name__ == '__main__':
    print encoding.to_utf8(u'꽃')
    main()
