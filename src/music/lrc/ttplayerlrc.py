#coding=utf-8
import os
import sys
sys.path.append('..')
import re
import random
from xml.dom.minidom import parse, parseString
from libs.convert import encoding


def CodeFunc(Id, data):
	length = len(data)
	
	tmp2=0
	tmp3=0
	
	tmp1 = (Id & 0x0000FF00) >> 8							#右移8位后为0x0000015F
															#tmp1 0x0000005F
	if ( (Id & 0x00FF0000) == 0 ):
		tmp3 = 0x000000FF & ~tmp1							#CL 0x000000E7
	else:
		tmp3 = 0x000000FF & ((Id & 0x00FF0000) >> 16)		#右移16位后为0x00000001
	
	tmp3 = tmp3 | ((0x000000FF & Id) << 8)					#tmp3 0x00001801
	tmp3 = tmp3 << 8										#tmp3 0x00180100
	tmp3 = tmp3 | (0x000000FF & tmp1)						#tmp3 0x0018015F
	tmp3 = tmp3 << 8										#tmp3 0x18015F00
	if ( (Id & 0xFF000000) == 0 ) :
		tmp3 = tmp3 | (0x000000FF & (~Id))					#tmp3 0x18015FE7
	else :
		tmp3 = tmp3 | (0x000000FF & (Id >> 24))			#右移24位后为0x00000000
	
	#tmp3	18015FE7
	
	i=length-1
	while(i >= 0):
		char = ord(data[i])
		if char >= 0x80:
			char = char - 0x100
		tmp1 = (char + tmp2) & 0x00000000FFFFFFFF
		tmp2 = (tmp2 << (i%2 + 4)) & 0x00000000FFFFFFFF
		tmp2 = (tmp1 + tmp2) & 0x00000000FFFFFFFF
		#tmp2 = (ord(data[i])) + tmp2 + ((tmp2 << (i%2 + 4)) & 0x00000000FFFFFFFF)
		i -= 1
	
	#tmp2 88203cc2
	i=0
	tmp1=0
	while(i<=length-1):
		char = ord(data[i])
		if char >= 128:
			char = char - 256
		tmp7 = (char + tmp1) & 0x00000000FFFFFFFF
		tmp1 = (tmp1 << (i%2 + 3)) & 0x00000000FFFFFFFF
		tmp1 = (tmp1 + tmp7) & 0x00000000FFFFFFFF
		#tmp1 = (ord(data[i])) + tmp1 + ((tmp1 << (i%2 + 3)) & 0x00000000FFFFFFFF)
		i += 1
	
	#EBX 5CC0B3BA
	
	#EDX = EBX | Id
	#EBX = EBX | tmp3
	tmp1 = (((((tmp2 ^ tmp3) & 0x00000000FFFFFFFF) + (tmp1 | Id)) & 0x00000000FFFFFFFF) * (tmp1 | tmp3)) & 0x00000000FFFFFFFF
	tmp1 = (tmp1 * (tmp2 ^ Id)) & 0x00000000FFFFFFFF
	
	if tmp1 > 0x80000000:
		tmp1 = tmp1 - 0x100000000
	return tmp1

def EncodeArtTit(str):
	rtn = ''
	str = encoding.to_unicode(str).encode('UTF-16')[2:]
	for i in range(len(str)):
		rtn += '%02x' % ord(str[i])
	
	return rtn

def getLrcUrl(Id, songname, artist):
	return 'http://lrcct2.ttplayer.com/dll/lyricsvr.dll?dl?Id=%d&Code=%d&uid=01&mac=%012x' % (int(Id),CodeFunc(int(Id), encoding.to_unicode(artist + songname).encode('UTF8')), random.randint(0,0xFFFFFFFFFFFF))

def parse(data):
	dom1 = parseString(data)
	lrclist = dom1.getElementsByTagName('lrc')
	result = []
	for node in lrclist:
		Id, songname, artist = node.getAttribute('id'), encoding.to_utf8(node.getAttribute('title')), encoding.to_utf8(node.getAttribute('artist'))
		
		songinfo = {
		'song_name': songname,
		'artist_name': artist,
		'album_name': '',
		'lrc': getLrcUrl(Id, songname, artist)
		}
		result.append(songinfo)
		pass
	return result

def search(songname, artist):
	if not artist:
		return
	from libs.utils import getUrlData
	theurl = 'http://lrcct2.ttplayer.com/dll/lyricsvr.dll?sh?Artist=%s&Title=%s&Flags=0' % (EncodeArtTit(artist.replace(' ','').lower()), EncodeArtTit(songname.replace(' ','').lower()))
	data = getUrlData(theurl)
	print repr(theurl)
	if data:
		return parse(data)
	pass

if __name__ == '__main__':
	result =  search('爱情', '王')
	print '共找到:', len(result)
	print '-------------------------------'
	i = 1
	for s in result:
		print '第', i, '首:'
		print '歌名：', encoding.to_utf8(s['song_name'])
		print '艺人：', encoding.to_utf8(s['artist_name'])
		print '专辑：', encoding.to_utf8(s['album_name'])
		print 'LRC: ', encoding.to_utf8(s['lrc'])
		print
		i += 1
		pass
	pass



