# -*- coding: utf-8 -*-
import sys

from libs.convert import encoding
from models.model import theLrcMgr


class Searcher(object):
    """docstring for Searcher"""

    @staticmethod
    def instance():
        if not hasattr(Searcher, '_instance'):
            Searcher._instance = Searcher()
        return Searcher._instance

    def __init__(self):
        self.reloadall()
        pass

    def reloadall(self):
        self.searchers = [item for item in theLrcMgr.get_all() if item['status'] == 1]
        pass

    def search(self, songname, artist=None, maxitem=20):
        result = []
        for s in self.searchers:
            try:
                # import qqlrc
                m = __import__(s['obj_name'], globals(), locals(), [], -1)
                data = m.search(songname, artist)
                if data:
                    result += data
                print len(result)
                if len(result) >= maxitem:
                    break
                pass
            except Exception, e:
                print e
                continue
        return result


def main():
    result = Searcher.instance().search('我喜欢你', '王羚柔')
    print '共找到:', len(result)
    print '-------------------------------'
    i = 1
    for s in result:
        print '第', i, '首:'
        print '歌名：', encoding.to_utf8(s['song_name'])
        print '艺人：', encoding.to_utf8(s['artist_name'])
        print '专辑：', encoding.to_utf8(s['album_name'])
        print 'LRC: ', encoding.to_utf8(s['lrc'])
        print
        i += 1
        pass
    pass


if __name__ == '__main__':
    main()
