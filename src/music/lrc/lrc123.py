# coding=utf-8
import os
import sys

sys.path.append('..')
import re
from libs.convert import encoding
from libs.utils import *

pattern = re.compile(r'歌手:<a.*?>(.*?)</a>.*?专辑:<a.*?>(.*?)</a>.*?歌曲:<a.*?>(.*?)</a>.*?<a href="(.*?)">LRC', re.DOTALL)


def format(s):
    return s.replace('<span class="highlighter">', '').replace('</span>', '')


def parse(data):
    # print data
    # print data
    match = pattern.findall(data)
    result = []
    for item in match:
        songinfo = {
            'artist_name': encoding.to_utf8(format(item[0])),
            'album_name': encoding.to_utf8(format(item[1])),
            'song_name': encoding.to_utf8(format(item[2])),
            'lrc': 'http://www.lrc123.com' + item[3]
        }
        result.append(songinfo)
        pass
    return result


def search(songname, artist):
    if artist and len(artist) > 0:
        theurl = "http://www.lrc123.com/?keyword=%s+%s&field=all" % (songname, artist)
    else:
        theurl = "http://www.lrc123.com/?keyword=%s&field=song" % songname
    print theurl
    data = getUrlData(theurl)
    if data:
        result = parse(data)
        return result
    return []


if __name__ == '__main__':
    result = search('爱情', '王')
    print '共找到:', len(result)
    print '-------------------------------'
    i = 1
    for s in result:
        print '第', i, '首:'
        print '歌名：', encoding.to_utf8(s['song_name'])
        print '艺人：', encoding.to_utf8(s['artist_name'])
        print '专辑：', encoding.to_utf8(s['album_name'])
        print 'LRC: ', encoding.to_utf8(s['lrc'])
        print
        i += 1
        pass
    pass
