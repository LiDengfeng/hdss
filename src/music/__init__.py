# -*- coding: utf-8 -*-

class Music(object):
    """
    歌曲信息
    """

    def __init__(self):
        self.id = None
        self.song_name = None
        self.artist_name = None
        self.album_name = None
        self.song_key = None
        self.artist_key = None
        self.album_key = None
        self.source = None  # 来源
        self.outer_url = None

    def __str__(self):
        return 'name: {song_name}, artist: {artist_name}, album: {album_name}, key: {song_key}, source: {source}, ' \
               'outer_url: {outer_url}'.format(
            song_name=self.song_name,
            artist_name=self.artist_name,
            album_name=self.album_name,
            song_key=self.song_key,
            source=self.source,
            outer_url=self.outer_url
        )

    def to_dict(self):
        d = {}
        d['id'] = self.id
        d['song_name'] = self.song_name
        d['artist_name'] = self.artist_name
        d['album_name'] = self.album_name
        d['song_key'] = self.song_key
        d['artist_key'] = self.artist_key
        d['album_key'] = self.album_key
        d['source'] = self.source
        d['outer_url'] = self.outer_url
        return d


def search_music(key, offset=0, limit=1, flag=0):
    """
    按关键字搜索音乐
    :param key:
    :param offset:
    :param limit:
    :param flag:
    :return:
    """
    if flag == 0:
        from music import Wangyiyun
        return Wangyiyun.search_music(key, offset, limit)
    elif flag == 1:
        from music import QQMusic
        return QQMusic.search_music(key, offset, limit)


def get_music_url(song_key):
    """
    根据id获取公网url
    :param song_key:
    :return:
    """
    if song_key.startswith('qq*$$*'):
        flag = 'qq'
    elif song_key.startswith('163*$$*'):
        flag = '163'
    else:
        flag = None

    if flag == '163':
        from music import Wangyiyun
        return Wangyiyun.get_music_resource(song_key)
    elif flag == 'qq':
        from music import QQMusic
        return QQMusic.get_music_resource(song_key)


def get_music_source_url(song_key):
    """
    获取原始url
    :param song_key:
    :return:
    """
    if song_key.startswith('qq*$$*'):
        flag = 'qq'
    elif song_key.startswith('163*$$*'):
        flag = '163'
    else:
        flag = None

    if flag == '163':
        return 'https://music.163.com/song?id={}'.format(song_key.split('*$$*')[1])
    elif flag == 'qq':
        return 'https://y.qq.com/n/yqq/song/{}.html'.format(song_key.split('*$$*')[1])
    else:
        return None


def get_music_toplist_list(flag):
    """
    获取排行榜
    :param flag:
    :return:
    """
    if flag == 0:
        from music import Wangyiyun
        return Wangyiyun.get_music_toplist_list()
    elif flag == 1:
        from music import QQMusic
        return QQMusic.get_music_toplist_list()


def search_lrc(key, offset=0, limit=1, flag=0):
    """
    按关键字搜索歌词
    :param key:
    :param offset:
    :param limit:
    :param flag:
    :return:
    """
    if flag == 0:
        pass
    elif flag == 1:
        from music import QQMusic
        return QQMusic.search_lrc(key, offset, limit)


def get_lrc(song_key):
    """
    下载歌词内容
    :param song_key:
    :return:
    """
    if song_key.startswith('qq*$$*'):
        flag = 'qq'
    elif song_key.startswith('163*$$*'):
        flag = '163'
    else:
        flag = None

    if flag == '163':
        return None
    elif flag == 'qq':
        from music import QQMusic
        return QQMusic.get_lrc(song_key)
    else:
        return None
