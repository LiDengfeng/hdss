# -*- coding: utf-8 -*-
"""
QQ音乐
"""
import base64
import json
import os
import re
import music
from libs import request as myrequest


def search_music(key, page=1, limit=1):
    """
    QQ音乐库搜索
    :param key:
    :param page:
    :param limit:
    :return:
    """
    url = 'https://c.y.qq.com/soso/fcgi-bin/client_search_cp'
    headers = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Origin": "https://y.qq.com",
        "Referer": "https://y.qq.com/portal/search.html",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36"
    }
    params = {
        'ct': 24,
        'qqmusic_ver': 1298,
        'new_json': 1,
        'remoteplace': 'txt.yqq.song',
        'searchid': '63780225736023230',
        't': 0,
        'aggr': 1,
        'cr': 1,
        'catZhida': 1,
        'lossless': 0,
        'flag_qc': 0,
        'p': page,
        'n': limit,
        'w': key,
        'g_tk': 5381,
        'loginUin': 0,
        'hostUin': 0,
        'format': 'json',
        'inCharset': 'utf8',
        'outCharset': 'utf-8',
        'notice': 0,
        'platform': 'yqq.json',
        'needNewCode': 0,
    }
    # 第一次返回：callback包
    response = myrequest.get(url, data=params, headers=headers)

    total_num = 0
    song_list = []
    if response and response.status_code == 200:
        html = response.text.strip("callback()[]")
        if html:
            jd = json.loads(html)
            if jd:
                song = jd['data']['song']
                total_num = song['totalnum']
                for x in song['list']:
                    d = music.Music()
                    d.source = 'qq'
                    d.song_name = x['name']
                    d.artist_name = x['singer'][0]['name']
                    d.album_name = x['album']['name']
                    d.song_key = 'qq*$$*' + str(x['mid']) + "*$$*" + x['file']['media_mid']
                    d.album_key = 'qq*$$*' + str(x['album']['id']) + "*$$*" + x['album']['mid']
                    d.artist_key = 'qq*$$*' + str(x['singer'][0]['id']) + "*$$*" + x['singer'][0]['mid']
                    d.id = str(x['id'])
                    song_list.append(d)

    return (total_num, song_list)


def get_music_resource(song_key):
    if "*$$*" in song_key:
        mid = song_key.split("*$$*")[1]
        media_mid = song_key.split("*$$*")[2]
        url = 'https://c.y.qq.com/base/fcgi-bin/fcg_music_express_mobile3.fcg?jsonpCallback=MusicJsonCallback&cid' \
              '=205361747&songmid=%s&filename=C400%s.m4a&guid=6612300644' % (mid, media_mid)
        html = myrequest.get(url)
        if html and html.status_code == 200:
            jd = html.json()
            if 'data' not in jd or 'items' not in jd['data']:
                return None, None
            if len(jd['data']['items']) == 0:
                return None, None
            vkey = jd['data']['items'][0]['vkey']
            if vkey:
                index_music_url = 'http://dl.stream.qqmusic.qq.com/{}' + media_mid + '.{}?vkey=' + vkey + '&guid=6612300644&uin=0&fromtag=66'
                music_type = {
                    'C400': 'm4a',
                    'M500': 'mp3',
                    'M800': 'mpe',
                    'A000': 'ape',
                    'F000': 'flac'
                }  # m4a, mp3普通, mp3高, ape, flac
                for k, v in music_type.items():
                    music_url = index_music_url.format(k, v)
                    html = myrequest.get(music_url)
                    if html and html.status_code == 200:
                        return music_url, v
    return None, None


def get_music_toplist_list():
    import time
    tnow = time.strftime("%Y-%m-%d", time.localtime())
    # url = 'https://u.y.qq.com/cgi-bin/musicu.fcg'
    url = 'https://u.y.qq.com/cgi-bin/musicu.fcg?-=getUCGI610716786599671&g_tk=5381&loginUin=0&hostUin=0&format=json&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq.json&needNewCode=0&data=%7B%22detail%22%3A%7B%22module%22%3A%22musicToplist.ToplistInfoServer%22%2C%22method%22%3A%22GetDetail%22%2C%22param%22%3A%7B%22topId%22%3A4%2C%22offset%22%3A0%2C%22num%22%3A20%2C%22period%22%3A%22{time}%22%7D%7D%2C%22comm%22%3A%7B%22ct%22%3A24%2C%22cv%22%3A0%7D%7D'
    headers = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Origin": "https://y.qq.com",
        "Referer": "https://y.qq.com/n/yqq/toplist/4.html",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36"
    }
    # 第一次返回：callback包
    response = myrequest.get(url.format(time=tnow), headers=headers)
    total_num = 0
    song_list = []
    if response and response.status_code == 200:
        html = response.text.strip("callback()[]")
        if html:
            jd = json.loads(html)
            if jd and 'code' in jd and jd['code'] == 0 and 'detail' in jd:
                # data = jd['detail']['data']['data']
                songInfoList = jd['detail']['data']['songInfoList']
                if not songInfoList or len(songInfoList) == 0:
                    songInfoList = jd['detail']['data']['data']['song']
                for x in songInfoList:
                    d = music.Music()
                    d.id = str(x['songId']) if 'songId' in x else str(x['id'])
                    d.source = 'qq'
                    d.song_name = x['name'] if 'name' in x else x['title']
                    d.artist_name = x['singer'][0]['name'] if 'singer' in x else x['singerName']
                    d.album_name = x['album']['name'] if 'album' in x else ""
                    if 'mid' in x:
                        d.song_key = 'qq*$$*' + str(x['mid']) + "*$$*" + x['file']['media_mid']
                        d.album_key = 'qq*$$*' + str(x['album']['id']) + "*$$*" + x['album']['mid']
                        d.artist_key = 'qq*$$*' + str(x['singer'][0]['id']) + "*$$*" + x['singer'][0]['mid']
                    elif 'ksong' in x and 'mid' in x['ksong']:
                        d.song_key = 'qq*$$*' + str(x['ksong']['mid']) + "*$$*" + x['file']['media_mid']
                        d.album_key = 'qq*$$*' + str(x['album']['id']) + "*$$*" + x['album']['mid']
                        d.artist_key = 'qq*$$*' + str(x['singer'][0]['id']) + "*$$*" + x['singer'][0]['mid']
                    else:
                        d.song_key = 'qq*$$*' + str("") + "*$$*" + ""
                        d.album_key = 'qq*$$*' + str("") + "*$$*" + ""
                        d.artist_key = 'qq*$$*' + str("") + "*$$*" + ""
                    song_list.append(d)

    return song_list


def search_lrc(key, page=1, limit=5):
    """
    QQ音乐库搜索
    :param key:
    :param page:
    :param limit:
    :return:
    """
    url = 'https://c.y.qq.com/soso/fcgi-bin/client_search_cp'
    headers = {
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Origin": "https://y.qq.com",
        "Referer": "https://y.qq.com/portal/search.html",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36"
    }
    params = {
        'ct': 24,
        'qqmusic_ver': 1298,
        'new_json': 1,
        'remoteplace': 'txt.yqq.lyric',
        'searchid': '96117889241186470',
        'aggr': 0,
        'catZhida': 1,
        'lossless': 0,
        'sem': 1,
        't': 7,
        'p': page,
        'n': limit,
        'w': key,
        'g_tk': 5381,
        'loginUin': 0,
        'hostUin': 0,
        'format': 'json',
        'inCharset': 'utf8',
        'outCharset': 'utf-8',
        'notice': 0,
        'platform': 'yqq.json',
        'needNewCode': 0,
    }
    # 第一次返回：callback包
    response = myrequest.get(url, data=params, headers=headers)

    total_num = 0
    lrc_list = []
    if response and response.status_code == 200:
        html = response.text.strip("callback()[]")
        if html:
            jd = json.loads(html)
            if jd:
                if 'data' in jd and 'lyric' in jd['data']:
                    total_num = jd['data']['lyric']['totalnum']
                    for x in jd['data']['lyric']['list']:
                        d = {}
                        d['title'] = x['title']
                        d['song_id'] = x['id']
                        d['song_mid'] = x['mid']
                        d['song_strMid'] = x['file']['strMediaMid']
                        d['singer'] = x['singer'][0]['name']
                        d['url'] = x['download_url']
                        d['song_key'] = 'qq*$$*' + str(d['song_mid']) + "*$$*" + str(d['song_id'])
                        d['id'] = str(x['id'])
                        lrc_list.append(d)

    return (total_num, lrc_list)


def get_lrc(song_key):
    """
    下载歌词内容
    :param song_key:
    :return:
    """
    if "*$$*" in song_key:
        song_mid = song_key.split("*$$*")[1]
        song_id = song_key.split("*$$*")[2]
        url = 'https://c.y.qq.com/lyric/fcgi-bin/fcg_query_lyric.fcg'
        headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Origin": "https://y.qq.com",
            "Referer": 'https://y.qq.com/n/yqq/song/{}.html'.format(song_mid),
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36"
        }
        params = {
            'musicid': song_id,
            'nobase64': '1',
            'callback': 'jsonp1',
            'g_tk': '5381',
            'jsonpCallback': 'jsonp1',
            'loginUin': '0',
            'hostUin': '0',
            'format': 'jsonp',
            'inCharset': 'utf8',
            'outCharset': 'utf-8',
            'notice': '0',
            'platform': 'yqq',
            'needNewCode': '0'
        }
        response = myrequest.get(url, data=params, headers=headers)
        if response and response.status_code == 200:
            html = response.text.strip("jsonp1()[]")
            jd = json.loads(html)
            if jd:
                lyric = jd['lyric']
                return lyric


def test():
    (count, songs) = search_music('后来')
    song_key = songs[0].song_key
    print (get_music_resource(song_key))


def test2():
    (count, lrc_list) = search_lrc('相思')
    for x in lrc_list:
        print (x)
        lyric = get_lrc(x['song_key'])
        print (lyric)
        break


if __name__ == '__main__':
    test()
    get_music_toplist_list()
    test2()
