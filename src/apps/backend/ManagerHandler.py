# -*- coding: utf-8 -*-
import os, sys
from hashlib import md5
from apps.basehandler import BaseHandler
from apps.route import route
from models import model
from config import theConfig
import tornado.web

# from lrc.searcher import Searcher

class ManagerBase(BaseHandler):
    def get_login_url(self):
        return '/manager/login'

    def get_current_user(self):
        name = self.get_secure_cookie('username')
        pwd = self.get_secure_cookie('userpw')
        if name and pwd:
            return model.theUserModel.get_user(name, pwd, True)
        return None

    def set_current_user(self, username, pwd, isencrpty=False):
        password = pwd
        if not isencrpty:
            password = md5(password.encode('utf-8')).hexdigest()
        self.set_secure_cookie('username', username, path="/", expires_days=1)
        self.set_secure_cookie('userpw', password, path="/", expires_days=1)
        pass

    def clear_user_cookie(self):
        self.clear_cookie('username')
        self.clear_cookie('userpw')

    def result(self, msg, tourl, issuccessed=True):
        flag = 'success' if issuccessed else 'error'
        return self.render('/manager/result', flag=flag, msg=msg, tourl=tourl)


@route(r'/manager/login')
class ManagerLogin(ManagerBase):
    """docstring for ManagerLogin"""

    def get(self):
        self.render('/manager/login')
        pass

    def post(self):
        try:
            name = self.get_argument("name")
            password = self.get_argument("pwd")

            susseccd = model.theUserModel.isexist_user(name, password)
            if susseccd:
                self.set_current_user(name, password)
                self.redirect('/manager/index')
                return
        except Exception as e:
            print (e)
        self.get()
        pass


@route(r'/manager/logout')
class ManagerLogout(ManagerBase):
    """docstring for ManagerLogout"""

    @tornado.web.authenticated
    def get(self):
        self.clear_cookie('username')
        self.clear_cookie('userpw')
        self.finish("<script>window.parent.location='/';</script>';")


@route(r'/admin')
class ManagerAdmin(ManagerBase):
    """docstring for ManagerAdmin"""

    def get(self):
        self.redirect('/manager/login')
        pass


@route(r'/manager')
@route(r'/manager/')
@route(r'/manager/index')
class Manager(ManagerBase):
    """docstring for Manager"""

    @tornado.web.authenticated
    def get(self):
        self.render('/manager/index')

@route(r'/manager/main')
class ManagerMain(ManagerBase):
    def get(self):
        import platform
        systeminfo = 'system:%s' % str(platform.uname())
        self.render('/manager/main', postcount=model.thePosts.count(), systeminfo=systeminfo)


@route(r'/manager/(.*).html')
class StaticFileHandler(ManagerBase):
    def get(self, frame):
        frames = ['top', 'menu']
        if frame in frames:
            self.render('/manager/' + frame)
        elif frame == 'main':
            self.redirect('/manager/main')
        else:
            template_path = self.settings['template_path']
            if os.path.exists(os.path.join(template_path, '/manager/' + frame + '.html')):
                self.render('/manager/' + frame)
            else:
                self.write("no " + frame + ".html")


@route(r'/manager/lrc')
class LrcMgr(ManagerBase):
    """docstring for LrcMgr"""

    @tornado.web.authenticated
    def get(self):
        self.render('/manager/lrcmgr', lrcfetcherlist=model.theLrcMgr.get_all())
        pass

    @tornado.web.authenticated
    def post(self):
        Id = self.get_argument('id', '')
        optype = self.get_argument('optype', '')
        if Id and optype:
            if optype == '0':
                model.theLrcMgr.enable(Id, True)
            elif optype == '1':
                model.theLrcMgr.enable(Id, False)
            elif optype == '2':
                model.theLrcMgr.remove_lrcobj(Id)
            elif optype == '4':
                name = self.get_argument('name', '')
                obj_name = self.get_argument('obj_name', '')
                id_num = self.get_argument('id_num', 0)
                if name and obj_name:
                    model.theLrcMgr.update_by_id(Id, {'name': name, 'obj_name': obj_name, 'id_num': id_num})
                    self.get()
                    pass

        elif optype == '3':
            name = self.get_argument('name', '')
            obj_name = self.get_argument('obj_name', '')
            id_num = self.get_argument('id_num', 0)
            if name and obj_name:
                model.theLrcMgr.add_lrcobj(name, obj_name, id_num)
                self.get()
                pass

        Searcher.instance().reloadall()
        pass


@route(r'/manager/lrcview')
class LrcView(ManagerBase):
    """docstring for LrcView"""

    @tornado.web.authenticated
    def get(self):
        page = int(self.get_argument('page', 1))
        songs = model.theSongs.getsongs_for_page(page - 1, 20)
        count = model.theSongs.count()
        self.render('/manager/lrcview', songs=songs, page=page, count=count)
        pass


@route(r'/manager/category')
@route(r'/manager/category/(\w+)/(\d+)')
class ManagerCategory(ManagerBase):
    """docstring for ManagerCategory"""

    @tornado.web.authenticated
    def get(self, op='', Id=0):
        category = model.theCategorys.get_category_by_id(Id)
        if 'delete' == op:
            model.theCategorys.remove_category_by_id(Id)

        categorys = model.theCategorys.get_all()
        self.render('/manager/category', caty=category, catslist=categorys)
        pass

    @tornado.web.authenticated
    def post(self):
        Id = self.get_argument('id', '')
        name = self.get_argument('name', '')
        id_num = self.get_argument('id_num', '')
        cattype = self.get_argument('cattype', '')
        url_path = self.get_argument('url_path', '')
        content = self.get_argument('content', '')

        if Id:
            model.theCategorys.update_category_by_id(Id, {'name': name, 'id_num': id_num, 'cattype': cattype,
                                                          'url_path': url_path, 'content': content})
        else:
            model.theCategorys.add_new_category(name, cattype, id_num, url_path, content)
        self.get()
        pass


@route(r'/manager/tags')
@route(r'/manager/tags/edit/(\d+)')
class ManagerTags(ManagerBase):
    """docstring for ManagerTags"""

    @tornado.web.authenticated
    def get(self, tagId=0):
        tag = model.theTags.get_by_id(tagId)
        tags = model.theTags.get_all()

        self.render('/manager/tags', tag=tag, tags=tags)
        pass

    @tornado.web.authenticated
    def post(self):
        tagId = self.get_argument('id', 0)
        tagname = self.get_argument('name', '默认')
        if tagId:
            model.theTags.update_tag(tagId, tagname)
        elif tagname:
            model.theTags.add_new_tag(tagname)
        self.get()
        pass


@route(r'/manager/postlist')
@route(r'/manager/postlist/(\w+)/(\d+)')
class ManagerPostList(ManagerBase):
    """docstring for ManagerPostList"""

    @tornado.web.authenticated
    def get(self, op='', page=1):
        if 'delete' == op:
            Id = self.get_argument('id', 0)
            model.thePosts.delete_by_id(Id)
            pass
        posts_cnt = model.thePosts.count(None, None, False)
        posts = model.thePosts.search('', '', int(page) - 1, 20, False)
        categorylist = model.theCategorys.get_all()
        categorys = {}
        for item in categorylist:
            categorys[item['id']] = item['name']
            pass

        for item in posts:
            item['category_name'] = '' if not item['id'] in categorys else categorys[item['id']]
            # item.add_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item.add_time))
            # item.edit_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item.edit_time))

            pass
        # data = {'postlist': posts, 'totalpost': posts_cnt, 'page': page, 'categorys':categorys}
        # import json
        # self.write(json.dumps(data))
        # return

        self.render('/manager/postlist',
                    postlist=posts,
                    totalpost=posts_cnt,
                    page=page,
                    categorys=categorys)
        pass


@route(r'/manager/post/new')
@route(r'/manager/post/(\d+)')
class ManagerPost(ManagerBase):
    """docstring for ManagerPost"""

    @tornado.web.authenticated
    def get(self, Id=0):
        apost = model.thePosts.get_by_id(Id)
        tags = model.theTags.get_all()
        categorys = model.theCategorys.get_all()

        self.render('/manager/post',
                    apost=apost,
                    tags=tags,
                    categorylist=categorys)
        pass

    @tornado.web.authenticated
    def post(self, Id=0):
        title = self.get_argument('title', '')
        category_id = self.get_argument('category_id', 0)
        content = self.get_argument('content', '')
        post_type = self.get_argument('post_type', 0)
        tags = self.get_argument('tags', '')
        status = self.get_argument('status', 0)
        ontop = self.get_argument('ontop', 0)
        closecomment = self.get_argument('closecomment', 0)

        if tags:
            tags = tags.replace(' ', '')
            taglist = filter(lambda x: x and len(x) > 0, list(set(tags.split(';'))))
            tags = ';'.join(taglist) + ';'
            #tags = tags[1:]
            pass

        apost = {
            'id': Id,
            'title': title,
            'category_id': category_id,
            'content': content,
            'post_type': post_type,
            'tags': tags,
            'status': status,
            'ontop': ontop,
            'closecomment': closecomment
        }
        if Id:
            model.thePosts.update_by_id(Id, apost)
        else:
            model.thePosts.add_new_post(apost)
            pass
        self.result('操作成功', '/manager/postlist')
        pass


@route(r'/manager/user')
class ManagerUser(ManagerBase):
    """docstring for ManagerUser"""

    @tornado.web.authenticated
    def get(self):
        user = self.get_current_user()
        self.render('/manager/user', username=user.name, msg='')
        pass

    @tornado.web.authenticated
    def post(self):
        user = self.get_current_user()
        oldpwd = self.get_argument('oldpwd', '')
        newusername = self.get_argument('newname', '')
        newpwd = self.get_argument('newpwd', '')
        repwd = self.get_argument('repwd', '')

        if user and user.password == md5(oldpwd).hexdigest():
            if newpwd:
                if newpwd == repwd:
                    if newusername:
                        model.theUser.update_by_id(user.id, newusername, newpwd)
                        self.set_current_user(newusername, newpwd)
                        self.render('/manager/user', username=newusername, msg='操作成功!!!')
                        pass
                    else:
                        model.theUser.updatepwd_by_id(user.id, newpwd)
                        self.set_current_user(user.name, newpwd)
                        self.render('/manager/user', username=user.name, msg='修改密码成功！！！')
                    pass
            elif newusername:
                model.theUser.updatename_by_id(user.id, newusername)
                self.set_current_user(newusername, user.password, True)
                self.render('/manager/user', username=newusername, msg='修改用户名成功!!!')
        else:
            self.get()
        pass


@route(r'/manager/links')
@route(r'/manager/links/(\w+)/(\d+)')
class ManagerLink(ManagerBase):
    """docstring for ManagerLink"""

    @tornado.web.authenticated
    def get(self, optype='', Id=0):
        if 'delete' == optype:
            model.theLinks.delete_by_id(Id)
            Id = 0
            pass

        alink = model.theLinks.get_by_id(Id)
        links = model.theLinks.get_all()

        self.render('/manager/links', alink=alink, links=links)
        pass

    @tornado.web.authenticated
    def post(self):
        Id = self.get_argument('id', '')
        name = self.get_argument('name', '')
        order = self.get_argument('order_id', '0')
        url = self.get_argument('url', '')
        content = self.get_argument('content', '')
        if not order:
            order = 0
        order = int(order)

        if name and url:
            if Id:
                model.theLinks.update_by_id(Id, {'name': name, 'order_id': order, 'url': url, 'content': content})
            else:
                model.theLinks.add_new_link(name, order, url, content)
            pass
        self.redirect('/manager/links')
        pass


@route(r'/manager/syssetting')
class ManagerSyssetting(ManagerBase):
    """docstring for ManagerSyssetting"""

    @tornado.web.authenticated
    def get(self):
        settings = model.theSysSetting.get_all_to_dic()
        settings['themes'] = ['default', 'blue', 'other']
        self.render('/manager/syssetting', settings=settings)
        pass

    @tornado.web.authenticated
    def post(self):
        webtitle = self.get_argument('webtitle', '')
        webdes = self.get_argument('webdes', '')
        webkeywords = self.get_argument('webkeywords', '')
        weburl = self.get_argument('weburl', '')
        webcdn = self.get_argument('webcdn', '')
        theme = self.get_argument('theme', 'default')
        tongji = self.get_argument('tongji', '')
        author = self.get_argument('author', '')

        model.theSysSetting.puts(author=author,
                                 webtitle=webtitle,
                                 webdes=webdes,
                                 webkeywords=webkeywords,
                                 weburl=weburl,
                                 webcdn=webcdn,
                                 theme=theme,
                                 tongji=tongji)
        self.redirect('/manager/syssetting')
        pass


@route(r'/manager/dbtable')
class ManagerDBTable(ManagerBase):
    """docstring for ManagerDBTable"""

    @tornado.web.authenticated
    def get(self):
        tableinfo = model.theTableMgr.gettableinfo()
        self.render('/manager/dbtablemgr', tableinfo=tableinfo)
        pass

    @tornado.web.authenticated
    def post(self):
        tbname = self.get_argument('tbname', '')
        optype = self.get_argument('optype', '')

        if tbname and optype:
            if 'clear' == optype:
                model.theTableMgr.cleartable(tbname)
                pass
            elif 'delete' == optype:
                model.theTableMgr.deletetable(tbname)
                pass
            pass
        pass


@route(r'/manager/pythonrun')
class ManagerPythonRun(ManagerBase):
    """docstring for PythonRun"""

    @tornado.web.authenticated
    def get(self):
        self.render('/manager/pythonrun')
        pass

    @tornado.web.authenticated
    def post(self):
        pythoncode = self.get_argument('content', '')
        if pythoncode:
            try:
                obj = compile(pythoncode, '', 'exec')
                print ('obj:', obj)
                result = eval(obj)
                print ('result:', result)
            except Exception as e:
                result = repr(e)
            pass
            self.write(repr(result))
        pass


@route(r'/manager/img')
class ManagerImg(ManagerBase):
    """docstring for ManagerImg"""

    @tornado.web.authenticated
    def get(self):
        islocal = ('LOCAL' == self.settings['runtime'])

        optype = self.get_argument('optype', '')
        name = self.get_argument('name', '')
        if name and 'delete' == optype:
            if islocal:
                dir = self.application.settings['upload_path']
                filename = os.path.join(dir, name)
                os.remove(filename)
                pass
            pass

        imgs = []
        if islocal:
            dir = self.application.settings['upload_path']
            imgs = os.listdir(dir)

            def func(item):
                ext = ['.jpg', '.jpeg', '.png', '.bmp', '.gif', '.tga', '.ico']
                if item == '.svn':
                    return False
                else:
                    for x in ext:
                        if item.endswith(x):
                            return True
                return False

            imgs = filter(func, imgs)

        self.render('/manager/img', imgs=imgs)
        pass

    @tornado.web.authenticated
    def post(self):
        islocal = ('LOCAL' == self.settings['runtime'])
        if islocal:
            upload_path = self.application.settings['upload_path']
            file_metas = self.request.files.get('img', [])
            for meta in file_metas:
                filename = os.path.basename(meta['filename'])
                filepath = os.path.join(upload_path, filename)
                with open(filepath, 'wb') as f:
                    f.write(meta['body'])
                self.get()
        pass
