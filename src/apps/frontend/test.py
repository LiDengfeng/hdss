# -*- coding: utf-8 -*-
from apps.basehandler import BaseHandler
from apps.route import route


@route(r'/test')
class TestIndex(BaseHandler):
    def get(self):
        username = self.get_argument('name', 'no')
        import time
        urllist = [
            ('https://www.kaiyuanzhognguo.com/', '开源中国'),
            ('https://www.baidu.com/', '百度'),
            ('https://www.zhihu.com/', '知乎'),
        ]
        self.render('test/test.html',
                    username=username,
                    time=time,
                    urllist=urllist,
                    )

@route(r'/base')
class Base(BaseHandler):
    def get(self):
        self.render('base')

@route(r'/extends')
class Base(BaseHandler):
    def get(self):
        self.render('test/extends')