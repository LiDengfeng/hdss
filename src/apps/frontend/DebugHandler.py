# -*- coding: utf-8 -*-
import os
from apps.basehandler import BaseHandler
from libs.convert import encoding
from apps.route import route


@route(r'/update')
class Handler(BaseHandler):
    def get(self):
        self.render("/update", msg="update from git...")
        self.update()

    def update(self):
        root_path = self.settings['root_path'].replace('\\', '/')
        print ("update:", root_path)
        from flib.command import Command
        cmd = Command("git pull " + root_path, logout=True, collectlog=True)
        if cmd: os.system("git fetch " + root_path)
        if cmd:
            self.render("/update", msg=encoding.to_utf8(str(cmd)), jump=1)
        else:
            self.render("/update", msg=encoding.to_utf8("update failed"), jump=1)

        # self.redirect('/index')


@route(r'/iloveyou')
class HandlerBirthday(BaseHandler):
    """
    happybirthday
    """

    def get(self):
        self.render('/static/birth_index')

@route(r'/sound/happy-birthday')
class HandlerSoundBirthday(BaseHandler):
    """
    happybirthday
    """

    def get(self):
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=' + 'happy-birthday.mp3')
        static_path = self.settings['static_path']
        filename = os.path.join(static_path, "birth", "happy-birthday.mp3")
        data = self.on_get_file(filename)
        if data:
            self.set_header('Content-Length', os.path.getsize(filename))
            self.write(data)
        self.flush()

    def on_get_file(self, filename):
        with open(filename, "r") as f:
            data = f.read()
            return data

@route(r'/love')
class LoveIndex(BaseHandler):
    """
    happybirthday
    """

    def get(self):
        self.render('/static/love')


@route(r'/lidengfeng')
class HandlerLidengfengIndex(BaseHandler):
    """
    lidengfeng
    """

    def get(self):
        self.render('/static/lidengfeng')


@route(r'/mine')
class HandlerMinegIndex(BaseHandler):
    """
    mine
    """

    def get(self):
        static_path = self.settings['static_path']
        file = os.path.join(static_path, "me.jpg")
        if not self.is_https():
            self.set_header('Content-Type', 'application/octet-stream')
        else:
            pass
        self.set_header('Content-Disposition', 'attachment; filename=me.jpg')
        buf_size = 4096
        with open(file, "rb") as f:
            while True:
                data = f.read(buf_size)
                if not data:
                    break
                self.write(data)
        self.finish()
        self.flush()


@route(r'/resume')
class HandlerResumeIndex(BaseHandler):
    """
    个人简历
    """

    def get(self):
        self.render('/static/resume')


@route(r'/aboutus')
class HandlerAboutusIndex(BaseHandler):
    """
        个人简历aboutus
        """

    def get(self):
        self.redirect('/resume')