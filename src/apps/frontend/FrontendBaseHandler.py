# -*- coding: utf-8 -*-

from hashlib import md5
from models import model
from apps.basehandler import BaseHandler


class FrontendBaseHandler(BaseHandler):
    """
    前端主页面
    """
    def render(self, templatename, **args):
        settings = model.theSysSetting.get_all_to_dic()
        socials = model.theSysSocial.get_all_to_dic()
        links = model.theLinks.get_all()
        if 'webtitle' in args: settings['webtitle'] = args['webtitle'] + ' |  ' + settings['webtitle']
        self.fill_render_args(args, 'settings', settings)
        self.fill_render_args(args, 'socials', socials)
        self.fill_render_args(args, 'links', links)
        from models import ui_methods
        self.fill_render_args(args, 'get_posts', ui_methods.get_posts)
        self.fill_render_args(args, 'get_categorys', ui_methods.get_categorys)
        self.fill_render_args(args, 'get_tags', ui_methods.get_tags)
        self.fill_render_args(args, 'get_newposlists', ui_methods.get_newposlists)
        # 当前登录用户
        user = self.get_current_user()
        self.fill_render_args(args, 'user', model.theUserModel.to_object(user) if user else None)

        r = super(FrontendBaseHandler, self).render
        r(templatename, **args)

    def get_current_user(self):
        name = self.get_secure_cookie('username')
        pwd = self.get_secure_cookie('userpw')
        if name and pwd:
            return model.theUserModel.get_user(name, pwd, True)
        return None

    def set_current_user(self, username, pwd, isencrpty=False):
        password = pwd
        if not isencrpty:
            password = md5(password.encode('utf-8')).hexdigest()
        self.set_secure_cookie('username', username, path="/", expires_days=1)
        self.set_secure_cookie('userpw', password, path="/", expires_days=1)
        pass

    def clear_user_cookie(self):
        self.clear_cookie('username')
        self.clear_cookie('userpw')
