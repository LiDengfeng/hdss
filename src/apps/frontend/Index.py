# -*- coding: utf-8 -*-
import json, re
import tornado.web

from apps.route import route
from libs.convert import encoding, dict_to_object
from models import model
from libs import utils
from .FrontendBaseHandler import FrontendBaseHandler


class NavigateBaseHandler(FrontendBaseHandler):
    """
    NavigateBaseHandler
    """

    def render(self, templatename, **args):
        if 'page' not in args:
            self.fill_render_args(args, 'page', 1)
        if 'count' not in args:
            self.fill_render_args(args, 'count', 1)
        self.fill_render_args(args, 'prepagecnt', self.get_prepagecnt())

        r = super(NavigateBaseHandler, self).render
        r(templatename, **args)
        pass

    def get_login_url(self):
        return '/login'

    def get_prepagecnt(self):
        return 5


class PostListBaseHandler(NavigateBaseHandler):
    """
    PostListBaseHandler
    """

    def render(self, page, typekey='', typevalue='', **args):
        postcnt = model.thePosts.count(typekey, typevalue)
        postlist = model.thePosts.search(typekey, typevalue, int(page) - 1, self.get_prepagecnt())
        categorylist = model.theCategorys.get_all()
        categorydic = utils.list_to_dic_by_idkey(categorylist)
        for apost in postlist:
            if apost['category_id'] in categorydic:
                apost['category_name'] = categorydic[apost['category_id']]['name']
            else:
                apost['category_name'] = None
            if apost['tags']:
                apost['tags'] = apost['tags'].split(';')

        newpostlist = model.thePosts.search('', '', 0, 8)
        # links = model.theLinks.get_all()

        args['postlist'] = postlist
        args['page'] = page
        args['count'] = postcnt
        args['newpostlist'] = newpostlist
        args['categorylist'] = categorylist
        # args['links'] = links

        r = super(PostListBaseHandler, self).render
        r('/blog/index', **args)
        pass


@route(r'/')
@route(r'/index')
class Index(PostListBaseHandler):
    def get(self):
        self.render(1)


@route(r'/logout')
class Logout(NavigateBaseHandler):
    """docstring for ManagerLogout"""

    @tornado.web.authenticated
    def get(self):
        self.clear_cookie('username')
        self.clear_cookie('userpw')
        self.finish("<script>window.parent.location='/';</script>';")


@route(r'/login')
class Login(FrontendBaseHandler):
    def get(self):
        self.render('auth/login')

    def post(self, *args, **kwargs):
        try:
            name = self.get_argument("name")
            password = self.get_argument("pwd")

            susseccd = model.theUserModel.isexist_user(name, password)
            if susseccd:
                self.set_current_user(name, password)
                next_url = self.get_argument('next', '/')
                self.redirect(next_url)
                return
        except Exception as e:
            print (e)
        self.get()
        pass


@route(r'/register')
class Register(FrontendBaseHandler):
    def get(self):
        self.render('auth/register')

    def post(self, *args, **kwargs):
        name = self.get_argument("name")
        email = self.get_argument("email")
        password = self.get_argument("pwd")
        model.theUserModel.add_new_user(name, password, email)
        self.set_current_user(name, password)
        next_url = self.get_argument('next', '/')
        self.redirect(next_url)
        return


@route(r'/music')
@route(r'/ymusic/api')
class MusicSearch(FrontendBaseHandler):
    def get(self):
        key = self.get_argument('key', None)
        optype = self.get_argument('type', None)
        flag = self.get_argument('flag', 1)
        flag = int(flag)

        if key:
            key = encoding.to_utf8(key)

        total_num = 0
        songlist = []
        if key:
            theCache = model.theCache
            cachedata = None
            if 'refresh' != optype:
                cachedata = theCache.get(key)
            if not cachedata:
                print ('no cache...')
                import music
                (total_num, songlist2) = music.search_music(key, offset=0, limit=18, flag=flag)
                for x in songlist2:
                    url, type = music.get_music_url(x.song_key)
                    x.outer_url = url
                    if 'json' == optype:
                        songlist.append(x.to_dict())
                    else:
                        songlist.append(dict_to_object(x.to_dict()))
            else:
                import music
                songlist2 = json.loads(cachedata)
                total_num = len(songlist)
                for x in songlist2:
                    if 'outer_url' not in x or x['outer_url'] is None:
                        url, type = music.get_music_url(x['song_key'])
                        x['outer_url'] = url
                    songlist.append(dict_to_object(x))

            if songlist and len(songlist) > 0:
                model.theSongs.addsongs(songlist)
                theCache.put(key, json.dumps(songlist).replace('\\', '\\\\'))
        else:
            import music
            songlist2 = music.get_music_toplist_list(flag)
            for x in songlist2:
                url, type = music.get_music_url(x.song_key)
                x.outer_url = url
                if 'json' == optype:
                    songlist.append(x.to_dict())
                else:
                    songlist.append(dict_to_object(x.to_dict()))
            total_num = len(songlist)
            if songlist and len(songlist) > 0:
                model.theSongs.addsongs(songlist)
        if 'json' == optype:
            outdata = encoding.to_utf8(json.dumps(songlist))
            self.write(outdata)
            return
        if self.request.path.startswith('/ymusic/api'):
            self.render('/ymusic/music', searchtext=key, searchlist=songlist, webtitle='音乐', total_num=total_num,
                        search_flag='music')
        else:
            self.render('/blog/music', searchtext=key, searchlist=songlist, webtitle='音乐', total_num=total_num,
                    search_flag='music')


@route(r'/music/download')
@route(r'/music/look')
class DownloadMusic(FrontendBaseHandler):
    def post(self, *args, **kwargs):
        song_key = self.get_argument('song_key', None)
        result = {'status': ""}
        if self.request.uri == '/music/download':
            if song_key:
                import music
                url, stype = music.get_music_url(song_key)
                if url:
                    result['status'] = "success"
                    result['url'] = url
                    result['song_key'] = song_key
                    result['filename'] = song_key + '.' + stype if stype is not None else song_key
                else:
                    result['status'] = "failed"
                    result['message'] = 'Failed to get url'
            else:
                result['status'] = "failed"
                result['message'] = 'Failed to get url, unknow song_key'
        elif self.request.uri == '/music/look':
            if song_key:
                import music
                url = music.get_music_source_url(song_key)
                if url:
                    result['status'] = "success"
                    result['url'] = url
                    result['song_key'] = song_key
                else:
                    result['status'] = "failed"
                    result['message'] = 'Failed to get url'
            else:
                result['status'] = "failed"
                result['message'] = 'Failed to get url, unknow song_key'
        self.write(result)


@route(r'/movie')
class Movie(FrontendBaseHandler):
    @tornado.web.authenticated
    def get(self, *args, **kwargs):
        try:
            test = self.get_argument('test', '0')
            if test != '1':
                self.render('/static/stop')
                return
            from libs import request as myrequest
            html = myrequest.get_html('https://cdn.111av.app/newsite.html')
            pattern = re.compile(
                '<a href="(.*?)" target="_blank" class="h5ui-btn h5ui-btn_primary">.*?</a>',
                re.S)
            result = re.findall(pattern, html)
            url = result[0]
            html = myrequest.search(url).read()
            pattern = re.compile(
                r'<div class="col-sm-6 col-md-4 col-lg-4">.*?<div class="well well-sm">.*?<a href="(.*?)">.*?img data-original="(.*?)" title="(.*?)".*?</div>')
            result = re.findall(pattern, html)
            videolist = []
            for x in result:
                d = {}
                d['url'] = url[0:-1] + x[0]
                d['img'] = x[1]
                d['name'] = x[2]
                videolist.append(d)
            self.render('/blog/movie', searchlist=videolist, webtitle='视频', total_num=len(videolist), search_flag='movie')
        except Exception as e:
            print("search error.", e)
            self.render('/blog/movie', searchlist=None, webtitle='视频', total_num=0, search_flag='movie')


@route(r'/blog')
@route(r'/blog/')
class BlogIndex(PostListBaseHandler):
    """docstring for BlogIndex"""

    def get(self):
        page = self.get_argument('page', '1')
        self.render(page)
        pass


@route(r'/blog/category/(\d+)')
class Category(PostListBaseHandler):
    """docstring for Category"""

    def get(self, category_id=0):
        page = self.get_argument('page', '1')
        self.render(page, 'category', category_id)
        pass


@route(r'/blog/tag/(.+)')
class Tag(PostListBaseHandler):
    """docstring for Tag"""

    def get(self, tagname=''):
        if tagname:
            page = self.get_argument('page', '1')
            self.render(page, 'tag', tagname)
        pass


@route(r'/search')
@route(r'/search/(.*)')
class Search(PostListBaseHandler):
    """docstring for Search"""

    def get(self, flag=None):
        s = self.get_argument('q', '', strip=True)
        if not s:
            self.redirect('/')
            return

        if not flag:
            page = self.get_argument('page', '1')
            self.render(page, 'title', s, q=s)
        elif flag == 'music':
            url = '/music?key='+s
            self.redirect(url)
        elif flag == 'lrc':
            url = '/lrcsearch?key=' + s
            self.redirect(url)


@route(r'/blog/post/(\d+)')
class Post(NavigateBaseHandler):
    """docstring for Post"""

    def get(self, Id=0):
        apost = model.thePosts.get_by_id(int(Id))
        newpostlist = model.thePosts.search('', '', 0, self.get_prepagecnt())
        categorylist = model.theCategorys.get_all()
        categorydic = utils.list_to_dic_by_idkey(categorylist)
        apost['category_name'] = None if not apost['category_id'] in categorydic else categorydic[apost['category_id']][
            'name']
        if apost['tags']:
            apost['tags'] = apost['tags'].split(';')

        links = model.theLinks.get_all()
        nextpost = model.thePosts.get_next(int(Id), False)
        prepost = model.thePosts.get_next(int(Id))

        related_posts = model.thePosts.get_by_category(apost['category_id'])

        firstpost = model.thePosts.get_first()
        lastpost = model.thePosts.get_last()

        self.render('/blog/post', apost=apost,
                    newpostlist=newpostlist,
                    categorylist=categorylist,
                    links=links,
                    nextpost=nextpost,
                    prepost=prepost,
                    related_posts=related_posts,
                    webtitle=apost.title,
                    firstpost=firstpost,
                    lastpost=lastpost)
        pass


@route(r'/lrcsearch')
class LrcSearch(FrontendBaseHandler):
    def get(self):
        key = self.get_argument('key', None)
        optype = self.get_argument('type', None)
        flag = self.get_argument('flag', 1)
        flag = int(flag)

        if key:
            key = encoding.to_utf8(key)
        import music
        (total_num, lrc_list) = music.search_lrc(key, offset=0, limit=5, flag=flag)
        outdata = encoding.to_utf8(json.dumps(lrc_list))
        self.write(outdata)

@route(r'/lrcdownload')
class LrcDownload(FrontendBaseHandler):
    def get(self):
        song_key = self.get_argument('song_key', None)
        optype = self.get_argument('type', None)
        flag = self.get_argument('flag', 1)
        flag = int(flag)
        import music
        content = music.get_lrc(song_key)
        if content:
            self.write(content)
        else:
            self.write("failed to get_lrc " + song_key)