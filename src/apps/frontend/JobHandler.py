# -*- coding: utf-8 -*-

import json
import os, sys
import logging
import re
import tornado.web
from models import model
from libs.convert import encoding
from libs import utils
from apps.basehandler import BaseHandler
from apps.route import route

@route(r'/job')
@route(r'/job/(.*)')
class JobHandler(BaseHandler):
    def get(self, frame = None):
        frames = ['weekline', 'workyear', 'company', 'education', 'word']
        if frame is None:
            self.render('/job/weekline', nav='weekline')
        elif frame in frames:
            self.render('/job/' + frame, nav=frame)

    def post(self, *args, **kwargs):
        pass
