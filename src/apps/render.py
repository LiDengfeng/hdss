# -*- coding: utf-8 -*-
import web
from config import theConfig
from libs.utils import *
web.config.debug = False

class Render(object):
	"""docstring for Render"""
	def __init__(self):
		super(Render, self).__init__()
		self.templatefunc = {}
		self.templatedata = {}

		self.templatefunc['include'] = self.include 
		pass

	def gettemplatedir(self):
		path = web.ctx.fullpath.lower()
		if path.startswith('/manager'):
			return 'templates/manager/'
		return 'templates/'

	def include(self, templatename):
		return self.render(templatename)

	def addtemplatefunc(self, func, quotfunc):
		self.templatefunc[func] = quotfunc
		pass	

	def render(self, templatename, **args):
		web.header("Content-Type","text/html; charset=utf-8")
		r = web.template.frender(os.path.join(self.gettemplatedir(), templatename + '.html'), globals=self.templatefunc)
		if args:
			return r(args)
		return r()

theRender = Render()		
