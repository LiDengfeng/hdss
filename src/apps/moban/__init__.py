# -*- coding: utf-8 -*-
import json, re
import tornado.web
from apps.basehandler import BaseHandler
from apps.route import route

class MobanBaseHandler(BaseHandler):
    pass

@route(r'/moban')
@route(r'/moban/index')
@route(r'/moban/index.html')
class HandlerMobanHome(MobanBaseHandler):
    """
    happybirthday
    """

    def get(self):
        self.render('/moban/index')

    def post(self):
        print ("post moban")
        
@route(r'/moban/about')       
class HandlerMobanAbout(MobanBaseHandler):
    """
    happybirthday
    """

    def get(self):
        self.render('/moban/about')
        
