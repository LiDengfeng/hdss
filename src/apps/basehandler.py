# -*- coding: utf-8 -*-
"""
所有处理的基类
"""
import sys
import time
import logging
import tornado.web
from mako.lookup import TemplateLookup
from mako.exceptions import RichTraceback
from config import theConfig

class BaseHandler(tornado.web.RequestHandler):
    """docstring for BaseHandler"""

    lookup = TemplateLookup([theConfig.settings['template_path']],
                                 input_encoding='utf-8',
                                 output_encoding='utf-8',
                                 default_filters=["none_to_blank", "unicode"],
                                 imports=['from libs.utils import none_to_blank',
                                          'from libs.utils import truncatewords',
                                          'from libs.utils import filter_html',
                                          'from libs.utils import filldefault',
                                          'from libs.utils import formatdate'])

    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)

    @classmethod
    def fill_render_args(cls, args, key, val):
        from libs import utils
        utils.filldefault(args, key, val)

    def render(self, template_name, **args):
        self.fill_render_args(args, 'static_url', self.static_url)
        self.fill_render_args(args, 'request', self)
        try:
            r = self.lookup.get_template(template_name if template_name.endswith('.html') else template_name + '.html')
            content = r.render(**args)
            self.write(content)
            self.flush()
        except:
            traceback = RichTraceback()
            for (filename, lineno, function, line) in traceback.traceback:
                print("File %s, line %s, in %s" % (filename, lineno, function))
                print(line, "\n")
            errinfo = "%s: %s" % (str(traceback.error.__class__.__name__), traceback.error)
            logging.error("render error: %s", errinfo)
        finally:
            pass

    def prepare(self):
        super(BaseHandler, self).prepare()
        pass

    def on_finish(self):
        super(BaseHandler, self).on_finish()
        pass

    def is_https(self):
        if self.request and self.request.protocol == "https":
            return True
        return False
