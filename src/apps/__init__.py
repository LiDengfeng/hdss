# -*- coding: utf-8 -*-
import os
import frontend
import backend
import moban
import basehandler
from route import route
#
#
@route(r'.*')
class PageNotFoundHandler(basehandler.BaseHandler):
    def get(self):
        uri = self.request.uri
        if uri.startswith('/'):
            if uri.endswith('.html'):
                p = self.settings['template_path'] + uri
                if os.path.exists(p):
                    self.render(uri)
                    return
            else:
                p = self.settings['static_path'] + uri
                if os.path.exists(p):
                    self.redirect('/static' + uri)
                    return
        import logging
        logging.error("page '%s' not found." % self.request.uri)
        self.render('404.html')


