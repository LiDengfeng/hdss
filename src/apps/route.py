# -*- coding: utf-8 -*-
class GlobalRoute(object):
    """
    把每个URL与Handler的关系保存到一个元组中，然后追加到列表内，列表内包含了所有的Handler
    """

    def __init__(self):
        self.urls = list()  # 路由列表

    def __call__(self, url, *args, **kwargs):
        def register(cls):
            self.urls.append((url, cls, kwargs))  # 把路由的对应关系表添加到路由列表中
            return cls

        return register

# 创建路由表对象
route = GlobalRoute()