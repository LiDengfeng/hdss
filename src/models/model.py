# -*- coding: utf-8 -*-
# MySQLdb
#

import copy
import itertools
import logging
import os, sys
import time

try:
    this_path = os.path.dirname(os.path.abspath(__file__))
except:
    this_path = os.path.dirname(os.path.abspath("__file__"))
root_path = os.path.abspath(os.path.join(this_path, os.path.pardir))
if root_path not in sys.path: sys.path.append(root_path)

from database import connector
from libs.convert import md5 as hashmd5
from libs.convert import encoding, dict_to_object
from config import theConfig

mdb = connector.Connection(host="%s:%s" % (theConfig.db_host, str(theConfig.db_port)),
                           database=theConfig.db_name,
                           user=theConfig.db_user,
                           password=theConfig.db_passwd,
                           max_idle_time=theConfig.db_timeout)
sdb = connector.Connection(host="%s:%s" % (theConfig.db_host, str(theConfig.db_port)),
                           database=theConfig.db_name,
                           user=theConfig.db_user,
                           password=theConfig.db_passwd,
                           max_idle_time=theConfig.db_timeout)


def defaultmodel(func):
    """
    defaultmodel
    :param func:
    :return:
    """
    def _dec(obj, *args, **kwargs):
        result = func(obj, *args, **kwargs)
        if not result:
            result = obj.model()
        return result

    return _dec


class BaseModel(object):
    """docstring for BaseModel"""
    def __init__(self):
        super(BaseModel, self).__init__()
        pass

    def model(self):
        if theConfig.db_type == 'sqlite':
            fields = sdb.query("PRAGMA table_info(" + self.tablename + ")")
            return dict_to_object(dict([[x.name, ""] for x in fields]))
        elif theConfig.db_type == 'mysql':
            fields = sdb.query("show columns from " + self.tablename)
            return dict_to_object(dict([[x.Field, ""] for x in fields]))
        else:
            pass

    def get_all(self):
        try:
            return [x for x in sdb.query('select * from ' + self.tablename)]
        except IndexError:
            pass

    def count(self):
        return sdb.query('select count(*)  as total from ' + self.tablename)[0]['total']


class TableMgr(BaseModel):
    """docstring for TableMgr"""
    def gettableinfo(self):
        sql = "SELECT TABLE_NAME,TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='%s';" % (
            theConfig.db_name)
        return mdb.query(sql)

    def deletetable(self, tname):
        sql = 'DROP TABLE %s;' % (tname)
        return mdb.execute(sql)

    def cleartable(self, tname):
        sql = 'delete from %s;' % (tname)
        b = mdb.execute(sql)
        if tname == 'tb_user':
            theUserModel.init_admin()
        return b


theTableMgr = TableMgr()


class KeyValue(BaseModel):
    """docstring for KeyValue"""
    def __init__(self, tablename='tb_keyvalue'):
        super(KeyValue, self).__init__()
        self.tablename = tablename
        if theConfig.db_type == 'sqlite':
            sql = '''
                    CREATE TABLE IF NOT EXISTS `%s`
                       (s_key varchar(64) PRIMARY KEY NOT NULL,
                       s_value  TEXT);
                    '''%(self.tablename)
        elif theConfig.db_type == 'mysql':
            sql = """
                    CREATE TABLE IF NOT EXISTS `%s` (
                    `s_key` varchar(64) NOT NULL,
                    `s_value` text,
                    UNIQUE KEY `s_key` USING BTREE (`s_key`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;
                """%(self.tablename)
        else:
            raise
        mdb.execute(sql)

    def makekey(self, skey):
        if sys.version_info < (3, 0):
            return encoding.to_utf8(skey)
        else:
            return skey

    def get(self, skey):
        key = self.makekey(skey)
        sql = 'SELECT * FROM %s WHERE s_key = $key;' % (self.tablename)
        results = sdb.get(sql, key=key)
        if results:
            return results['s_value']

    def get_all_to_dic(self):
        results = self.get_all()
        if results:
            outdict = {}
            for item in results:
                outdict[item['s_key']] = item['s_value']
            return outdict
        pass

    def put(self, skey, svalue):
        self.clear(skey)
        sql = 'INSERT INTO %s (s_key, s_value) VALUES($key, $value);' % (self.tablename)
        return mdb.execute(sql, key=self.makekey(skey), value=svalue)

    def puts(self, **agrs):
        for item in agrs:
            self.clear(item)
            self.put(item, agrs[item])

    def clear(self, skey):
        key = self.makekey(skey)
        sql = 'DELETE FROM %s WHERE s_key = $key;' % (self.tablename)
        return mdb.execute(sql, key=key)

    def clearall(self):
        sql = 'DELETE FROM %s;' % (self.tablename)
        return mdb.execute(sql)

    def isexist(self, skey):
        if self.get(skey):
            return True
        return False


theCache = KeyValue()


class SysSetting(KeyValue):
    """docstring for SysSetting"""

    def __init__(self):
        super(SysSetting, self).__init__('tb_syssetting')
        self.puts(author='libyyu@qq.com',
                  webtitle='fn2018',
                  webdes='李登峰的个人网站',
                  webkeywords='python,go886,c\c++,程序设计,语言,搏客',
                  weburl='http://www.libyyu.com',
                  webdomain='libyyu.com',
                  webcdn='',
                  theme='default',
                  tongji='',
                  createtime='2019-10-27',
                  record_info='京ICP备18055182号',
                  development_info='个人',
                  arrange_info='阿里云')


theSysSetting = SysSetting()

class SysSocial(KeyValue):
    """docstring for SysSetting"""

    def __init__(self):
        super(SysSocial, self).__init__('tb_syssocial')
        self.puts(github='https://github.com/libyyu',
                  weibo='https://weibo.com/',
                  zhihu='https://www.zhihu.com/',
                  qq='389465209',
                  wechat='FnLove2010')


theSysSocial = SysSocial()

class Songs(BaseModel):
    """docstring for Songs"""

    def __init__(self):
        super(Songs, self).__init__()
        self.tablename = 'tb_songs'
        if theConfig.db_type == 'mysql':
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_songs` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `song_name` text NOT NULL,
              `artist_name` text,
              `album_name` text,
              `song_key` text NOT NULL,
              `lrc` text,
              `lrc_content` text,
              PRIMARY KEY (`id`),
              UNIQUE KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == 'sqlite':
            sql = '''
                CREATE TABLE IF NOT EXISTS `tb_songs`
                   (id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                   song_name           TEXT    NOT NULL,
                   artist_name       TEXT,
                   album_name       TEXT,
                   song_key       TEXT    NOT NULL,
                   lrc       TEXT,
                   lrc_content       TEXT);
                '''
        else:
            raise
        mdb.execute(sql)
        pass

    def get_song(self, songId):
        sql = 'SELECT * FROM tb_songs WHERE id=%s;'
        return sdb.get(sql, songId)

    def get_song_by_songkey(self, songkey):
        sql = 'SELECT * FROM tb_songs WHERE song_key=%s;'
        return sdb.get(sql, songkey)

    def isexist(self, songkey):
        sql = 'SELECT COUNT(*) AS total FROM tb_songs WHERE song_key=%s;'
        return sdb.get(sql, songkey)['total'] > 0

    def addsong(self, s, song_key=None):
        if song_key:
            songkey = song_key
        elif 'song_key' in s:
            songkey = s['song_key']
        else:
            songkey = hashmd5('%s-%s-%s' % (s['song_name'], s['artist_name'], s['album_name'])).hexdigest()
        existsong = self.get_song_by_songkey(songkey)
        if existsong:
            return existsong['id']

        sql = 'INSERT INTO tb_songs (song_name, artist_name, album_name, song_key, lrc) VALUES(%s,%s,%s,%s,%s);'
        return mdb.execute(
            sql,
            encoding.to_utf8(s['song_name']),
            encoding.to_utf8(s['artist_name']),
            encoding.to_utf8(s['album_name']),
            encoding.to_utf8(songkey),
            s['lrc'] if 'lrc' in s else "")

    def addsongs(self, songs):
        if songs and len(songs) > 0:
            for s in songs:
                s['id'] = self.addsong(s)
        pass

    def updatesong_lrc(self, songId, lrc):
        sql = 'UPDATE tb_songs SET lrc=%s WHERE id=%s;'
        return mdb.execute(sql, encoding.to_utf8(lrc), songId)

    def updatesong_lrcContent(self, songId, lrcContent):
        sql = 'UPDATE tb_songs SET lrc_content=%s WHERE id=%s;'
        return mdb.execute(sql, encoding.to_utf8(lrcContent), songId)

    def getsongs_for_page(self, page=0, prepagenum=20):
        sql = "SELECT * FROM tb_songs ORDER BY id ASC LIMIT %s, %s;"
        begindex = int(page) * int(prepagenum)
        return sdb.query(sql, begindex, prepagenum)


theSongs = Songs()


class User(BaseModel):
    """docstring for User"""

    def __init__(self):
        super(User, self).__init__()
        self.tablename = 'tb_user'
        if theConfig.db_type == 'mysql':
            sql = """
                CREATE TABLE IF NOT EXISTS `tb_user` (
                `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(32) NOT NULL DEFAULT '',
                `email` varchar(32) DEFAULT '',
                `password` varchar(32) NOT NULL DEFAULT '',
                `role` smallint(6) unsigned DEFAULT 0,
                PRIMARY KEY (`id`),
                UNIQUE KEY `name` (`name`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == 'sqlite':
            sql = '''
                CREATE TABLE IF NOT EXISTS `tb_user`
                   (id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                   name           TEXT    NOT NULL,
                   email           TEXT ,
                   password       TEXT    NOT NULL,
                   role INTEGER DEFAULT 0);
                '''
        else:
            raise

        mdb.execute(sql)
        self.init_admin()

    def init_admin(self):
        if not self.has_user():
            self.add_new_user('admin', 'admin', 'libyyu@qq.com', 1)

    def has_user(self):
        try:
            sql = 'SELECT id FROM %s LIMIT 1;' % (self.tablename)
            return sdb.get(sql)
        except:
            pass

    def get_user(self, name, pw, isencrpty=False):
        if name and pw:
            user = self.get_user_by_name(name)
            if user:
                pwd = pw
                if not isencrpty:
                    pwd = hashmd5(pw).hexdigest()
                if user.name == name and user.password == pwd:
                    return user

    class SimpleUser(object):
        def __init__(self, data):
            self.kw = {}
            for (k, v) in data.items():
                if k != 'password':
                    self.kw[k] = v

        def __getattr__(self, key):
            return self.kw[key]

        @property
        def get_unread_count(self):
            return 1

    @classmethod
    def to_object(cls, user):
        return User.SimpleUser(user)

    def get_user_by_name(self, name):
        try:
            sql = 'SELECT * FROM tb_user WHERE name=$name;'
            return sdb.get(sql, name=name)
        except:
            pass

    def add_new_user(self, name='', pw='', email='', role=0):
        if name and pw:
            try:
                pwd = hashmd5(pw).hexdigest()
                sql = 'INSERT INTO tb_user (name, password, email, role) VALUES($name, $pwd, $email, $role);'
                return mdb.execute(sql, name=name, pwd=pwd, email=email, role=role)
            except:
                pass

    def isexist_user(self, name='', pw='', isencrpty=False):
        user = self.get_user(name, pw, isencrpty)
        return user

    def update_by_id(self, Id, name, pwd, email, role=0):
        sql = 'UPDATE tb_user SET name=%s password=%s email=%s role=%d WHERE id=%s;'
        return mdb.execute(sql, name, hashmd5(pwd).hexdigest(), email, role, Id)

    def updatepwd_by_id(self, Id, pwd):
        sql = 'UPDATE tb_user SET password=%s WHERE id=%s;'
        return mdb.execute(sql, hashmd5(pwd).hexdigest(), Id)


theUserModel = User()


class Links(BaseModel):
    """docstring for Links"""

    def __init__(self):
        super(Links, self).__init__()
        self.tablename = 'tb_links'
        if theConfig.db_type == 'mysql':
            sql = """CREATE TABLE IF NOT EXISTS `tb_links` (
              `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
              `order_id` tinyint(3) NOT NULL DEFAULT '0',
              `name` varchar(100) NOT NULL DEFAULT '',
              `url` varchar(200) NOT NULL DEFAULT '',
              `content` varchar(200) NOT NULL DEFAULT '',
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_links`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                order_id       INT     NOT NULL DEFAULT 0,
                name           TEXT    NOT NULL DEFAULT '',
                url            TEXT    NOT NULL DEFAULT '',
                content        TEXT    NOT NULL DEFAULT '');
            """
        else:
            raise
        mdb.execute(sql)
        pass

    @defaultmodel
    def get_by_id(self, Id):
        if Id:
            sql = 'SELECT * FROM tb_links WHERE id=%s;'
            return sdb.get(sql, Id)
        return self.model()

    def add_new_link(self, name, order_id, url, content=''):
        sql = 'INSERT INTO tb_links (name, order_id, url , content) VALUES(%s,%s,%s,%s);'
        return mdb.execute(sql, name, order_id, url, content)

    def delete_by_id(self, Id):
        sql = 'DELETE FROM tb_links WHERE id=%s;'
        return mdb.execute(sql, Id)

    def update_by_id(self, Id, alink):
        sql = 'UPDATE tb_links SET name=%s, order_id=%s, url=%s, content=%s WHERE id=%s;'
        return mdb.execute(sql,
                           alink['name'],
                           alink['order_id'],
                           alink['url'],
                           alink['content'],
                           Id)


theLinks = Links()


class Comments(BaseModel):
    """docstring for Comments"""

    def __init__(self):
        super(Comments, self).__init__()
        self.tablename = 'tb_commnets'
        if theConfig.db_type == 'mysql':
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_comments` (
              `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
              `postid` mediumint(8) unsigned NOT NULL DEFAULT '0',
              `author` varchar(20) NOT NULL,
              `email` varchar(30) NOT NULL,
              `url` varchar(75) NOT NULL,
              `visible` tinyint(1) NOT NULL DEFAULT '1',
              `add_time` int(10) unsigned NOT NULL DEFAULT '0',
              `content` mediumtext NOT NULL,
              PRIMARY KEY (`id`),
              KEY `postid` (`postid`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_comments`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                postid       INT     NOT NULL DEFAULT 0,
                author           TEXT    NOT NULL,
                email            TEXT    NOT NULL,
                url        TEXT    NOT NULL,
                visible       INT     NOT NULL DEFAULT 1,
                add_time       INT     NOT NULL DEFAULT 0,
                content        TEXT    NOT NULL);
            """
        else:
            raise
        mdb.execute(sql)
        pass


theComments = Comments()


class Category(BaseModel):
    """docstring for Category"""

    def __init__(self):
        super(Category, self).__init__()
        self.tablename = 'tb_category'
        if theConfig.db_type == 'mysql':
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_category` (
              `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(17) NOT NULL DEFAULT '',
              `cattype` tinyint(4) NOT NULL DEFAULT '0',
              `id_num` mediumint(8) unsigned NOT NULL DEFAULT '0',
              `url_path` varchar(17) NOT NULL DEFAULT '',
              `content` mediumtext NOT NULL,
              PRIMARY KEY (`id`),
              KEY `name` (`name`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_category`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                name       TEXT     NOT NULL DEFAULT '',
                cattype           INT    NOT NULL DEFAULT 0,
                id_num            INT    NOT NULL DEFAULT 0,
                url_path        TEXT    NOT NULL DEFAULT '',
                content        TEXT    NOT NULL);
            """
        else:
            raise
        mdb.execute(sql)
        pass

    def add_new_category(self, name, cattype=0, id_num=0, url_path='', content=''):
        sql = 'INSERT INTO tb_category (name, cattype, id_num, url_path, content) VALUES(%s,%s,%s,%s,%s);'
        return mdb.execute(sql,
                           name,
                           cattype,
                           id_num,
                           url_path,
                           content)

    @defaultmodel
    def get_category_by_id(self, Id):
        sql = 'SELECT * FROM tb_category WHERE id=%s;'
        return sdb.get(sql, Id)

    def remove_category_by_id(self, Id):
        sql = 'DELETE FROM tb_category WHERE id=%s;'
        return mdb.execute(sql, Id)

    def update_category_by_id(self, Id, category):
        sql = 'UPDATE tb_category SET name = %s, cattype=%s, id_num=%s, url_path=%s, content=%s WHERE id=%s;'
        return mdb.execute(sql,
                           category['name'],
                           category['cattype'],
                           category['id_num'],
                           category['url_path'],
                           category['content'],
                           Id)


theCategorys = Category()


class Posts(BaseModel):
    """docstring for Posts"""

    def __init__(self):
        super(Posts, self).__init__()
        self.tablename = 'tb_posts'
        if theConfig.db_type == 'mysql':
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_posts` (
              `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
              `category_id` mediumint(8) NOT NULL DEFAULT '0',
              `post_type` varchar(4) NOT NULL DEFAULT '0',
              `title` varchar(100) NOT NULL DEFAULT '',
              `content` mediumtext NOT NULL,
              `ontop` tinyint(1) NOT NULL DEFAULT '0',
              `status` tinyint(4) NOT NULL DEFAULT '0',
              `comment_num` mediumint(8) unsigned NOT NULL DEFAULT '0',
              `closecomment` tinyint(1) NOT NULL DEFAULT '0',
              `tags` varchar(100) NOT NULL,
              `password` varchar(8) NOT NULL DEFAULT '',
              `add_time` int(10) unsigned NOT NULL DEFAULT '0',
              `edit_time` int(10) unsigned NOT NULL DEFAULT '0',
              `read_count` int(10) unsigned NOT NULL DEFAULT '0',
              `love_count` int(10) unsigned NOT NULL DEFAULT '0',
              `hate_count` int(10) unsigned NOT NULL DEFAULT '0',
              `comments_count` int(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              KEY `category_id` (`category_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_posts`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                category_id       INT     NOT NULL DEFAULT 0,
                post_type           TEXT    NOT NULL DEFAULT '0',
                title            TEXT    NOT NULL DEFAULT '',
                content        TEXT    NOT NULL,
                ontop        INT    NOT NULL DEFAULT 0,
                status        INT    NOT NULL DEFAULT 0,
                comment_num        INT    NOT NULL DEFAULT 0,
                tags        TEXT    NOT NULL,
                password        TEXT    NOT NULL DEFAULT '',
                add_time        INT    NOT NULL DEFAULT 0,
                edit_time        INT    NOT NULL DEFAULT 0,
                read_count        INT    NOT NULL DEFAULT 0,
                love_count        INT    NOT NULL DEFAULT 0,
                hate_count        INT    NOT NULL DEFAULT 0,
                comments_count        INT    NOT NULL DEFAULT 0);
            """
        else:
            raise
        mdb.execute(sql)
        pass

    def add_new_post(self, apost):
        now = time.time()
        sql = 'INSERT INTO tb_posts (category_id, post_type, title, content, ontop, status, closecomment, tags, add_time, edit_time) \
            VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'
        return mdb.execute(sql,
                           apost['category_id'],
                           apost['post_type'],
                           apost['title'],
                           apost['content'],
                           apost['ontop'],
                           apost['status'],
                           apost['closecomment'],
                           apost['tags'],
                           now,
                           now)

    def delete_by_id(self, Id):
        sql = 'DELETE FROM tb_posts WHERE id=%s;'
        return mdb.execute(sql, Id)

    def update_by_id(self, Id, apost):
        sql = 'UPDATE tb_posts SET category_id=%s, post_type=%s, title=%s, content=%s, ontop=%s, status=%s, ' \
              'closecomment=%s, tags=%s, edit_time=%s, read_count=%s, love_count=%s, hate_count=%s, comments_count=%s ' \
              'WHERE id=%s; '
        return mdb.execute(sql,
                           apost['category_id'],
                           apost['post_type'],
                           apost['title'],
                           apost['content'],
                           apost['ontop'],
                           apost['status'],
                           apost['closecomment'],
                           apost['tags'],
                           time.time(),
                           apost['read_count'] if 'read_count' in apost else 0,
                           apost['love_count'] if 'love_count' in apost else 0,
                           apost['hate_count'] if 'hate_count' in apost else 0,
                           apost['comments_count'] if 'comments_count' in apost else 0,
                           Id)

    @defaultmodel
    def get_by_id(self, Id):
        sql = 'SELECT * FROM tb_posts WHERE id=%s;'
        return sdb.get(sql, Id)

    def get_first(self):
        sql = 'SELECT id, title FROM tb_posts WHERE status=1 ORDER BY id DESC LIMIT 1;'
        return sdb.get(sql)

    def get_last(self):
        sql = 'SELECT id, title FROM tb_posts WHERE status=1 ORDER BY id ASC LIMIT 1;'
        return sdb.get(sql)

    def get_next(self, Id, isnext=True):
        if isnext:
            sql = 'SELECT id, title FROM tb_posts WHERE id>%s AND status=1 ORDER BY id ASC LIMIT 1;'
        else:
            sql = 'SELECT id, title FROM tb_posts WHERE id<%s AND status=1 ORDER BY id DESC LIMIT 1;'

        return sdb.get(sql, Id)

    def get_by_category(self, category_id=0, limit=5):
        if category_id:
            sql = 'SELECT id, title FROM tb_posts WHERE category_id=%s AND status=1 ORDER BY id DESC LIMIT %s;'
            return sdb.query(sql, category_id, limit)
        else:
            sql = 'SELECT id, title FROM tb_posts ORDER BY id DESC LIMIT %s;'
            return sdb.query(sql, limit)

    def count(self, types='', key=None, filter=True):
        sql = None
        if filter:
            if types == 'tag':
                sql = "SELECT count(id) AS total FROM %s WHERE tags like '%%%s%%' AND status = 1 ;" % (
                    self.tablename, '%' + str(key) + '%')
            elif types == 'title':
                sql = "SELECT count(id) AS total FROM %s WHERE title like '%%%s%%' AND status = 1 ;" % (
                    self.tablename, '%' + str(key) + '%')
            elif types == 'category':
                sql = "SELECT count(id) AS total FROM %s WHERE category_id=%d AND status = 1 ;" % (
                    self.tablename, int(key))
            else:
                sql = "SELECT count(id) AS total FROM %s WHERE status = 1;" % (self.tablename)
        else:
            if types == 'tag':
                sql = "SELECT count(id) AS total FROM %s WHERE tags like '%%%s%%';" % (
                    self.tablename, '%' + str(key) + '%')
            elif types == 'title':
                sql = "SELECT count(id) AS total FROM %s WHERE title like '%%%s%%';" % (
                    self.tablename, '%' + str(key) + '%')
            elif types == 'category':
                sql = "SELECT count(id) AS total FROM %s WHERE category_id=%d;" % (self.tablename, int(key))
            else:
                sql = "SELECT count(id) AS total FROM %s;" % (self.tablename)

        if sql:
            return sdb.get(sql)['total']
        return 0

    def search(self, types='', value=None, page=0, prepagenum=20, filter=True):
        sql = None
        begindex = str(int(page) * int(prepagenum))
        num = str(prepagenum)
        if filter:
            if types == 'tag':
                sql = "SELECT * FROM %s WHERE tags like '%%%s%%' AND status = 1 ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, '%' + str(value) + '%', begindex, num)
            elif types == 'title':
                sql = "SELECT * FROM %s WHERE title like '%%%s%%' AND status = 1 ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, '%' + str(value) + '%', begindex, num)
            elif types == 'category':
                sql = "SELECT * FROM %s WHERE category_id=%d AND status = 1 ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, int(value), begindex, num)
            else:
                sql = "SELECT * FROM %s WHERE status = 1 ORDER BY ontop DESC, id DESC LIMIT %s, %s;" % (
                    self.tablename, begindex, num)
                pass
        else:
            if types == 'tag':
                sql = "SELECT * FROM %s WHERE tags like '%%%s%%' ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, '%' + str(value) + '%', begindex, num)
            elif types == 'title':
                sql = "SELECT * FROM %s WHERE title like '%%%s%%' ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, '%' + str(value) + '%', begindex, num)
            elif types == 'category':
                sql = "SELECT * FROM %s WHERE category_id=%d ORDER BY id DESC LIMIT %s, %s;" % (
                    self.tablename, int(value), begindex, num)
            else:
                sql = "SELECT * FROM %s ORDER BY id DESC LIMIT %s, %s;" % (self.tablename, begindex, num)
        if sql:
            return sdb.query(sql)
        return list()

    def _update_read_count(self, Id, filedname, count):
        sql = 'UPDATE tb_posts SET %s=%d WHERE id=%s;'
        return mdb.execute(sql, filedname, count, Id)

    def inc_read_count(self, Id, count=1):
        info = self.get_static_countinfo(Id)
        return self.update_read_count(Id, 'read_count', info['read_count'] + count)

    def inc_love_count(self, Id, count=1):
        info = self.get_static_countinfo(Id)
        return self.update_read_count(Id, 'love_count', info['love_count'] + count)

    def inc_hate_count(self, Id, count=1):
        info = self.get_static_countinfo(Id)
        return self.update_read_count(Id, 'hate_count', info['hate_count'] + count)

    def inc_comments_count(self, Id, count=1):
        info = self.get_static_countinfo(Id)
        return self.update_read_count(Id, 'comments_count', info['comments_count'] + count)

    def get_static_countinfo(self, Id):
        """
        获取统计信息
        :param Id:
        :return:
        """
        sql = 'SELECT read_count, love_count, hate_count, comments_count from tb_posts WHERE id=%s;'
        return sdb.get(sql, Id)




thePosts = Posts()


class Tags(BaseModel):
    """docstring for Tags"""

    def __init__(self):
        super(Tags, self).__init__()
        self.tablename = 'tb_tags'
        if theConfig.db_type == "mysql":
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_tags` (
              `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(17) NOT NULL DEFAULT '',
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_tags`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                name       TEXT     NOT NULL DEFAULT '');
            """
        else:
            raise
        mdb.execute(sql)
        pass

    def add_new_tag(self, name):
        sql = 'INSERT INTO tb_tags (name) VALUES(%s);'
        return mdb.execute(sql, name)

    def update_tag(self, Id, name):
        sql = 'UPDATE tb_tags SET name=%s WHERE id=%s;'
        return mdb.execute(sql, name, Id)

    @defaultmodel
    def get_by_id(self, Id):
        sql = 'SELECT * FROM tb_tags WHERE id=%s;'
        return sdb.get(sql, str(Id))

    def get_by_name(self, name):
        try:
            sql = 'SELECT * FROM tb_tags WHERE name=%s;'
            return sdb.get(sql, name)
        except IndexError:
            return self.model()
        pass


theTags = Tags()


class LrcMgr(BaseModel):
    """docstring for LrcMgr"""

    def __init__(self):
        super(LrcMgr, self).__init__()
        self.tablename = 'tb_lrcmgr'
        if theConfig.db_type == "mysql":
            sql = """
            CREATE TABLE IF NOT EXISTS `tb_lrcmgr` (
              `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
              `id_num` mediumint(8) unsigned NOT NULL DEFAULT '0',
              `name` varchar(32) NOT NULL DEFAULT '',
              `obj_name` varchar(32) NOT NULL DEFAULT '',
              `content` mediumtext,
              `status` tinyint(1) NOT NULL DEFAULT '1',
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            """
        elif theConfig.db_type == "sqlite":
            sql = """CREATE TABLE IF NOT EXISTS `tb_lrcmgr`(
                id INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT,
                id_num       INT     NOT NULL DEFAULT 0,
                name       TEXT     NOT NULL DEFAULT '',
                obj_name       TEXT     NOT NULL DEFAULT '',
                content       TEXT,
                status       INT     NOT NULL DEFAULT 1);
            """
        else:
            raise
        mdb.execute(sql)
        pass

    def get_all(self):
        try:
            sql = 'SELECT * FROM tb_lrcmgr ORDER BY id_num ASC;'
            return sdb.query(sql)
        except IndexError:
            pass
        pass

    @defaultmodel
    def get_by_id(self, Id):
        sql = 'SELECT * FROM tb_lrcmgr WHERE id=%s;'
        return sdb.get(sql, Id)

    def add_lrcobj(self, name, obj_name, id_num=0, content=''):
        sql = 'INSERT INTO tb_lrcmgr (name, obj_name, content, id_num) VALUES(%s,%s,%s,%s);'
        return mdb.execute(sql, name, obj_name, content, id_num)

    def remove_lrcobj(self, Id):
        sql = 'DELETE FROM tb_lrcmgr WHERE id=%s;'
        return mdb.execute(sql, str(Id))

    def enable(self, Id, enabled=True):
        sql = 'UPDATE tb_lrcmgr SET status=%s WHERE id=%s;'
        return mdb.execute(sql, enabled, Id)

    def update_by_id(self, Id, obj):
        sql = 'UPDATE tb_lrcmgr SET name=%s, id_num=%s, obj_name=%s WHERE id=%s;'
        return mdb.execute(sql,
                           obj['name'],
                           obj['id_num'],
                           obj['obj_name'],
                           Id)


theLrcMgr = LrcMgr()
