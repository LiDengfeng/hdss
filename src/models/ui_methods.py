# -*- coding: utf-8 -*-
from models import model
from libs import utils

def get_categorys():
    categorylist = model.theCategorys.get_all()
    return categorylist

def get_tags():
    taglist = model.theTags.get_all()
    return taglist

def get_newposlists():
    newpostlist = model.thePosts.search('', '', 0, 5)
    return newpostlist

def get_posts(page, typekey='', typevalue='', **args):
    postcnt = model.thePosts.count(typekey, typevalue)
    postlist = model.thePosts.search(typekey, typevalue, int(page) - 1, 5)
    categorylist = model.theCategorys.get_all()
    categorydic = utils.list_to_dic_by_idkey(categorylist)
    for apost in postlist:
        if apost['category_id'] in categorydic:
            apost['category_name'] = categorydic[apost['category_id']]['name']
        else:
            apost['category_name'] = None
        if apost['tags']:
            apost['tags'] = apost['tags'].split(';')

    newpostlist = model.thePosts.search('', '', 0, 8)
    links = model.theLinks.get_all()

    args['postlist'] = postlist
    args['page'] = page
    args['count'] = postcnt
    args['newpostlist'] = newpostlist
    args['categorylist'] = categorylist
    args['links'] = links
    return args
