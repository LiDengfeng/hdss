# -*- coding: utf-8 -*-
# 提供操作浏览器接口
import sys
import os
import re
import zlib
import json
from gzip import GzipFile
import requests

# 解决python版本兼容问题
if sys.version_info < (3, 0):
    PY3 = False
elif sys.version_info >= (3, 0):
    PY3 = True

if not PY3:
    reload(sys)
    sys.setdefaultencoding('utf8')
    try:
        import cStringIO as StringIO
    except ImportError:
        from StringIO import StringIO
    import urllib
    import urlparse
    import cookielib
    from urllib2 import BaseHandler, addinfourl, HTTPCookieProcessor, build_opener, install_opener, HTTPHandler, \
        HTTPSHandler
else:
    from io import BytesIO as StringIO
    from http import cookiejar as cookielib
    from urllib.request import BaseHandler, HTTPCookieProcessor, build_opener, install_opener, HTTPHandler, HTTPSHandler
    from urllib.response import addinfourl


def deflate(data):
    try:
        return zlib.decompress(data, -zlib.MAX_WBITS)
    except zlib.error:
        return zlib.decompress(data)


def urlquote(s):
    # return urlparse.unquote(s)
    return urllib.quote(s)


class ContentEncodingProcessor(BaseHandler):
    """docstring for ContentEncodingProcessor"""

    def http_request(self, req):
        # req.add_header('Accept-Encoding', 'gzip, deflate')
        # req.add_header('Transfer-encoding', 'gzip, deflate')
        return req

    def http_response(self, req, resp):
        old_resp = resp
        if resp.headers.get('content-encoding') == 'gzip':
            gz = GzipFile(fileobj=StringIO(resp.read()), mode='r')
            resp = addinfourl(gz, old_resp.headers, old_resp.url, old_resp.code)
            resp.msg = old_resp.msg
        if resp.headers.get('content-encoding') == 'deflate':
            gz = StringIO(deflate(resp.read()))
            resp = addinfourl(gz, old_resp.headers, old_resp.url, old_resp.code)
            resp.msg = old_resp.msg
        return resp


DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    # "Accept-Encoding": "gzip, deflate",
}


def getOpener():
    """
    获取浏览器
    :return:
    """
    cj = cookielib.CookieJar()
    cookie_support = HTTPCookieProcessor(cj)
    encoding_support = ContentEncodingProcessor()
    opener = build_opener(cookie_support, encoding_support, HTTPHandler, HTTPSHandler)
    install_opener(opener)
    return opener

def getWebDriver():
    """
    获取webdriver
    :return:
    """
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    chrome_opt = Options()  # 创建参数设置对象.
    chrome_opt.add_argument('--no-sandbox')  # root用户不加这条会无法运行
    chrome_opt.add_argument('--headless')  # 无界面化.
    chrome_opt.add_argument('--disable-gpu')  # 配合上面的无界面化.
    chrome_opt.add_argument('--window-size=1366,768')  # 设置窗口大小, 窗口大小会有影响.
    chrome_opt.add_argument('–-disable-infobars')  # 禁用浏览器正在被自动化程序控制的提示
    # 创建Chrome对象并传入设置信息.
    driver = webdriver.Chrome(chrome_options=chrome_opt)
    return driver

_g_opener = None


def search(url, data=None, headers=DEFAULT_HEADERS, timeout=10 * 3):
    """
    搜索网页
    :param url:
    :param data:
    :param headers:
    :param timeout:
    :return:
    """
    global _g_opener
    if not _g_opener:
        _g_opener = getOpener()
    if headers:
        _headers = []
        for (k, v) in headers.items():
            _headers.append((k, v))
        _g_opener.addheaders = _headers
    if not PY3 and data:
        data = urllib.urlencode(data)
    elif PY3 and data:
        data = bytes(json.dumps(data), encoding="utf-8")
    response = _g_opener.open(url, data=data, timeout=timeout)
    return response


def get_html(url, headers=None):
    """
    抓取网页内容
    :param url:
    :param headers:
    :return:
    """
    if not headers:
        headers = DEFAULT_HEADERS
    resp = search(url, headers=headers)
    return resp.read()


def get_page(url, frame=None, waitid=None, waitcls=None):
    """
    抓取浏览器内容
    :param url:
    :return:
    """
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    # 创建Chrome对象并传入设置信息.
    driver = getWebDriver()
    # 操作这个对象.
    driver.get(url)
    try:
        if frame:
            driver.switch_to.frame(frame)
        # 创建wait对象.
        if waitid:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, waitid)))
        if waitcls:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, waitcls)))
        html = driver.page_source
        return html
    finally:
        driver.quit()


def post(url, data=None, headers=None):
    r = requests.session()
    if not headers:
        headers = DEFAULT_HEADERS
    html = r.post(url, data=data, headers=headers)
    return html


def get(url, data=None, headers=None):
    r = requests.session()
    if not headers:
        headers = DEFAULT_HEADERS
    html = r.get(url, params=data, headers=headers)
    return html


def test():
    def get_content(offset):
        url = 'http://www.200avs.com/'
        a = search(url)  # 打开网址
        html = a.read()
        return html

    def get_html(url):
        r = get(url)
        html = r.text
        return html

    def get(html):
        reg = re.compile(
            r'<li>.*?<p.*?class="dec">.*?<.*?title="(.*?)".*?href="(.*?)".*?>.*?span class="s-fc4".*?title="(.*?)".*?href="(.*?)".*?</li>',
            re.S)  # 匹配换行符
        items = re.findall(reg, html)
        return items

    html = get_content(0)
    with open("tmp.html", "wb") as f:
        f.write(html)
    items = get(html)


def test2():
    header = {
        "Cookie": "JSESSIONID=ABAAABAAADEAAFIF0390879C909ED6B25B79466EE283BE3; _ga=GA1.2.417433648.1513319104; _gid=GA1.2.1618013105.1513319104; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1513319105; user_trace_token=20171215142351-84d77e04-e160-11e7-9a0b-525400f775ce; LGSID=20171215142351-84d782ed-e160-11e7-9a0b-525400f775ce; PRE_UTM=m_cf_cpt_baidu_pc; PRE_HOST=bzclk.baidu.com; PRE_SITE=http%3A%2F%2Fbzclk.baidu.com%2Fadrc.php%3Ft%3D06KL00c00f7Ghk60yUKm0FNkUs0dpWPp00000PW4pNb00000zJBmtg.THL0oUhY1x60UWdBmy-bIfK15yRvujDYnvf3nj0snADdrAm0IHYznDNAf1mdPbD3wjDvPjmkn1c3PHfsPYf3nRfsP16zw0K95gTqFhdWpyfqn101n1csPHnsPausThqbpyfqnHm0uHdCIZwsT1CEQLILIz4_myIEIi4WUvYE5LNYUNq1ULNzmvRqUNqWu-qWTZwxmh7GuZNxTAn0mLFW5HRLrjDL%26tpl%3Dtpl_10085_15730_11224%26l%3D1500117464%26attach%3Dlocation%253D%2526linkName%253D%2525E6%2525A0%252587%2525E9%2525A2%252598%2526linkText%253D%2525E3%252580%252590%2525E6%25258B%252589%2525E5%25258B%2525BE%2525E7%2525BD%252591%2525E3%252580%252591%2525E5%2525AE%252598%2525E7%2525BD%252591-%2525E4%2525B8%252593%2525E6%2525B3%2525A8%2525E4%2525BA%252592%2525E8%252581%252594%2525E7%2525BD%252591%2525E8%252581%25258C%2525E4%2525B8%25259A%2525E6%25259C%2525BA%2526xp%253Did%28%252522m6c247d9c%252522%29%25252FDIV%25255B1%25255D%25252FDIV%25255B1%25255D%25252FDIV%25255B1%25255D%25252FDIV%25255B1%25255D%25252FH2%25255B1%25255D%25252FA%25255B1%25255D%2526linkType%253D%2526checksum%253D220%26ie%3Dutf-8%26f%3D8%26tn%3Dbaidu%26wd%3D%25E6%258B%2589%25E5%258B%25BE%25E7%25BD%2591%26oq%3D%2525E6%25259D%2525AD%2525E5%2525B7%25259E%2525E7%252594%2525B5%2525E5%2525AD%252590%2525E7%2525A7%252591%2525E6%25258A%252580%2525E5%2525A4%2525A7%2525E5%2525AD%2525A6%26rqlang%3Dcn%26inputT%3D2518; PRE_LAND=https%3A%2F%2Fwww.lagou.com%2F%3Futm_source%3Dm_cf_cpt_baidu_pc; LGUID=20171215142351-84d785f4-e160-11e7-9a0b-525400f775ce; X_HTTP_TOKEN=352f0849051a7bc84ed05f3ba6808fe9; _putrc=7E09D7EE60C821B7; login=true; unick=%E5%90%B4%E4%BF%8A%E4%BA%9A; showExpriedIndex=1; showExpriedCompanyHome=1; showExpriedMyPublish=1; hasDeliver=0; index_location_city=%E5%8C%97%E4%BA%AC; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1513319596; LGRID=20171215143204-aa3d51ef-e161-11e7-9a28-525400f775ce; TG-TRACK-CODE=index_search",
        "Host": "www.lagou.com",
        'Origin': 'https://www.lagou.com',
        'Referer': 'https://www.lagou.com/jobs/list_%E6%95%B0%E6%8D%AE%E5%88%86%E6%9E%90?city=%E5%8C%97%E4%BA%AC&cl=false&fromSearch=true&labelWords=&suginput=',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'}
    data = {'first': 'true', 'pn': 1, 'kd': '开发工程师'}
    url = 'http://www.lagou.com/jobs/positionAjax.json?needAddtionalResult=false'
    resp = search(url, headers=header, data=data)
    data = resp.read()
    print (data)
    with open('common_handler_baidu.html', 'wb') as f:
        f.write(data)
    jdata = json.loads(data)
    print (jdata)


def test3():
    # 获取原码
    def get_content(page):
        url = 'http://search.51job.com/list/000000,000000,0000,00,9,99,python,2,' + str(page) + '.html'
        print (url)
        a = search(url)  # 打开网址
        html = a.read()
        from libs.convert import encoding
        return encoding.to_utf8(html)

    def get(html):
        reg = re.compile(
            r'class="t1 ">.*? <a target="_blank" title="(.*?)".*? <span class="t2"><a target="_blank" title="(.*?)".*?<span class="t3">(.*?)</span>.*?<span class="t4">(.*?)</span>.*? <span class="t5">(.*?)</span>',
            re.S)  # 匹配换行符
        items = re.findall(reg, html)
        return items

    # 多页处理，下载到文件
    for j in range(1, 10):
        print("正在爬取第" + str(j) + "页数据...")
        html = get_content(j)  # 调用获取网页原码
        for i in get(html):
            with open('51job.txt', 'a') as f:
                f.write(i[0] + '\t' + i[1] + '\t' + i[2] + '\t' + i[3] + '\t' + i[4] + '\n')
                f.close()


if __name__ == '__main__':
    test()
