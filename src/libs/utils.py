# -*- coding: utf-8 -*-
import os
import sys
import re
import time
from functools import wraps

re_lrc = re.compile(r'\[\d{2}:\d{2}.\d{2}\]')


def islrc(data):
    """
    是否满足歌词
    :param data:
    :return:
    """
    return re_lrc.search(data)


def singleton(cls):
    """
    单例
    使用装饰器(decorator),
    这是一种更pythonic,更elegant的方法,
    单例类本身根本不知道自己是单例的,因为他本身(自己的代码)并不是单例的
    """
    instances = {}

    @wraps(cls)
    def _singleton(*args, **kw):
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


def import_driver(drivers, preferred=None):
    """Import the first available driver or preferred driver.
    """
    if preferred:
        drivers = [preferred]

    for d in drivers:
        try:
            return __import__(d, None, None, ['x'])
        except ImportError:
            pass
    raise ImportError("Unable to import " + " or ".join(drivers))


def import_path(paths):
    """import absolute path
    """
    for dirpath in paths:
        if os.path.isfile(dirpath):
            (dirpath, _) = os.path.split(dirpath)
        if dirpath not in sys.path: sys.path.append(dirpath)


def import_file(file):
    """Import file with absolute path
    """
    (dirpath, filename) = os.path.split(file)
    (fname, _) = os.path.splitext(filename)
    if dirpath not in sys.path: sys.path.append(dirpath)
    return import_driver([fname])

def formatdate(value):
    if not value:
        return ''
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(value)))


def none_to_blank(value):
    if value is None:
        return ""
    return value


def truncatewords(value):
    return value[:140]


def list_to_dic_by_idkey(objlist):
    result = {}
    for item in objlist:
        result[item.id] = item
    return result


def filldefault(amap, key, value=''):
    if key not in amap: amap[key] = value
    pass


# 用了HTMLParser，有更简单的方式吗？正则？
def filter_html(htmltext):
    if sys.version_info < (3, 0):
        from HTMLParser import HTMLParser
    else:
        from html.parser import HTMLParser
    html = htmltext.strip()
    html = htmltext.strip("\n")
    result = []
    parser = HTMLParser()
    parser.handle_data = result.append
    parser.feed(html)
    parser.close()
    return ''.join(result)
