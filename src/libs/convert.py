# -*- coding: utf-8 -*-
import sys

try:
    import chardet
except ImportError:
    from flib import chardet


class dict_to_object(dict):
    """docstring for dict_to_object"""

    def __getattr__(self, key):
        try:
            return self[key]
        except:
            return ''
        pass

    def __setattr__(self, key, value):
        self[key] = value
        pass


class encoding(object):
    """
    编码转换
    """
    PY3 = sys.version_info >= (3, 0)

    @classmethod
    def get_encoding(cls, text):
        """
        获取text的编码格式
        :param text:
        :return:
        """
        try:
            return chardet.detect(text)
        except:
            pass

    @classmethod
    def is_unicode(cls, text):
        if not cls.PY3 and isinstance(text, unicode):
            return True
        elif cls.PY3 and isinstance(text, str):
            return True
        else:
            return False

    @classmethod
    def is_utf8(cls, text):
        ec = cls.get_encoding(text)
        if ec and ec['encoding'] == 'utf-8':
            return True

    @classmethod
    def to_unicode(cls, text):
        """
        转换为unicode
        :param text:
        :return:
        """
        if not text:
            return
        if not cls.PY3 and isinstance(text, unicode):
            return text
        elif cls.PY3 and isinstance(text, str):
            return text
        try:
            enc = chardet.detect(text)['encoding']
            if enc == 'ISO-8859-2' or enc == "TIS-620" or enc is None:
                enc = 'gbk'
                if not cls.PY3:
                    return unicode(text, enc, 'ignore')
                elif cls.PY3:
                    return text.decode('utf-8', 'ignore')
        except Exception as e:
            return

        charsets = ('gbk', 'gb18030', 'gb2312', 'iso-8859-1', 'utf-16', 'utf-8', 'utf-32', 'ascii')
        for charset in charsets:
            try:
                if not cls.PY3:
                    return unicode(text, charset)
                elif cls.PY3:
                    return text.decode(charset)
            except:
                continue

    @classmethod
    def to_utf8(cls, text):
        """
        转换为utf8
        :param text:
        :return:
        """
        try:
            if not cls.PY3:
                if not isinstance(text, unicode) and not isinstance(text, str) and not isinstance(text, bytes):
                    return text or ""
            elif cls.PY3:
                if not isinstance(text, str) and not isinstance(text, bytes):
                    return
            if not cls.PY3 and isinstance(text, unicode):
                return text.encode('utf-8')
            elif cls.PY3 and isinstance(text, str):
                return text.encode('utf-8')
            enc = chardet.detect(text)['encoding']
            if enc == 'utf-8':
                return text
            else:
                s = cls.to_unicode(text)
                if s:
                    return s.encode('utf-8')
        except Exception as e:
            return ""

    @classmethod
    def to_gb2312(cls, text):
        """
        转成gb2312
        :param text:
        :return:
        """
        try:
            enc = cls.get_encoding(text)
            if enc and enc['encoding'] == "utf-8":
                text = text.decode('utf-8')
        except Exception as e:
            pass
        s = cls.to_unicode(text)
        if s:
            return s.encode('gb2312')

    @classmethod
    def to_gbk(cls, text):
        """
        转成gbk
        :param text:
        :return:
        """
        try:
            enc = cls.get_encoding(text)
            if enc and enc['encoding'] == "utf-8":
                text = text.decode('utf-8')
        except Exception as e:
            pass
        s = cls.to_unicode(text)
        if s:
            return s.encode('gbk')

    @classmethod
    def to_ansi(cls, text):
        """
        转成ANSI
        :param text:
        :return:
        """
        try:
            enc = cls.get_encoding(text)
            if enc and enc['encoding'] == "utf-8":
                text = text.decode('utf-8')
        except Exception as e:
            pass
        s = cls.to_unicode(text)
        if s:
            return s.encode('ascii')

    @classmethod
    def to_str(cls, text):
        if not text: return ""
        try:
            if not cls.PY3:
                if not isinstance(text, unicode) and not isinstance(text, str) and not isinstance(text, bytes):
                    return str(text)
            else:
                if not isinstance(text, str) and not isinstance(text, bytes):
                    return str(text)
                if isinstance(text, str):
                    return text
                elif isinstance(text, bytes):
                    return text.decode('utf-8')
            if sys.stdout.encoding == "cp936":
                return cls.to_gbk(text)
            return cls.to_utf8(text)
        except Exception as e:
            return text

def md5(str):
    from hashlib import md5 as builtin_md5
    return builtin_md5(encoding.to_utf8(str))