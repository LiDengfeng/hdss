# -*- coding: utf-8 -*-
import os, sys
try:
    this_path = os.path.dirname(os.path.abspath(__file__))
except:
    this_path = os.path.dirname(os.path.abspath("__file__"))

if this_path not in sys.path: sys.path.append(this_path)