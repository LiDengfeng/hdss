# -*- coding: utf-8 -*-
#
import os
import sys

try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
except:
    pass
try:
    import tornado.wsgi
    import tornado.ioloop
    import tornado.web
    import tornado.options
    import tornado.httpserver
    from tornado.options import define, options
except Exception as e:
    raise e
from config import theConfig
from apps.route import route
import logging
# 定义服务器监听端口，可通过终端传递--port=8001
define("port", default=8001, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        tornado.web.Application.__init__(self, route.urls, **theConfig['settings'])


def main(args):
    try:
        tornado.options.parse_command_line(args)
        app = tornado.httpserver.HTTPServer(Application(), theConfig['ssl_options'])
        app.listen(options.port)
        logging.info('listen port %d', options.port)
        tornado.ioloop.IOLoop.instance().start()
    except Exception as e:
        tornado.ioloop.IOLoop.instance().stop()
        logging.exception(str(e))
        quit()


if __name__ == '__main__':
    main(None)
