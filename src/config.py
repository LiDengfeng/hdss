# -*- coding: utf-8 -*-
#
import os
import platform
import logging
import tornado.log
from libs.convert import dict_to_object

__author__ = 'lidengfeng<libyyu@qq.com>'
__version__ = '0.0.1beta'
__license__ = 'MIT'

try:
    this_path = os.path.dirname(__file__)
except:
    this_path = os.path.dirname("__file__")
root_path = os.path.abspath(os.path.join(this_path, os.path.pardir))
# tornado参数配置
settings = dict(
    root_path=os.path.abspath(root_path),
    src_path=this_path,
    template_path=os.path.join(root_path, "templates"),
    static_path=os.path.join(root_path, "static"),
    upload_path=os.path.join(root_path, "static/uploads"),
    data_path=os.path.join(root_path, "data"),
    cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
    login_url="/login",
    runtime='LOCAL',
)

ssl_options = dict(
    certfile=os.path.join(root_path, "ssl/2585338_hdss.libyyu.com.pem"),
    keyfile=os.path.join(root_path, "ssl/2585338_hdss.libyyu.com.key"),
)

if platform.system() == "Windows":
    settings['debug'] = True
else:
    settings['debug'] = False

config = {
    'version': __version__,
    'is_debug': settings['debug'],
    'is_cache': False,
    'cache_time': 60 * 60 * 7,
    'db_type': 'mysql',
    'db_name': 'fn2018',
    'db_host': '127.0.0.1',
    'db_port': 3306,
    'db_timeout': 60 * 60 * 7,
    'db_user': 'root',
    'db_passwd': '199010',
    'db_prefix': 'fn2018_',
    'settings': settings,
    'ssl_options': ssl_options
}


# 日志格式
logging.getLogger().setLevel(logging.INFO)
formatter = logging.Formatter(fmt="[%(levelname).4s %(asctime)s %(name)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
handler = logging.StreamHandler()
handler.setFormatter(formatter)
logging.getLogger().addHandler(handler)


def log_request(request_handler):
    """
    tornado请求日志
    :param request_handler:
    :return:
    """
    if request_handler.get_status() == 200:
        log_method = tornado.log.access_log.info
    elif request_handler.get_status() in [302, 500]:
        log_method = tornado.log.access_log.warning
    else:
        log_method = tornado.log.access_log.error

    request_time = 1000.0 * request_handler.request.request_time()
    log_method("%d %s %.2fms %s", request_handler.get_status(), request_handler._request_summary(), request_time,
               request_handler.request.headers.get("User-Agent", ""))


# 自定义tornado日志消息格式
settings["log_function"] = log_request

logging.info(config["version"])
logging.info(config['db_type'])
logging.info(settings['runtime'])

theConfig = dict_to_object(config)